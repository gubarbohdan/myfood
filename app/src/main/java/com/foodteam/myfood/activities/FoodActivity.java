package com.foodteam.myfood.activities;

import android.support.v7.app.AppCompatActivity;

import com.foodteam.myfood.utils.FragmentInteractionListener;
import com.foodteam.myfood.utils.Navigator;

/**
 * Created by gubar on 25.02.2016.
 */
public class FoodActivity extends AppCompatActivity implements FragmentInteractionListener {
    @Override
    public void onBackToMyFood() {
        Navigator.getInstance().showMyFoodFragment(FoodActivity.this);
    }

    @Override
    public void onBackToRecipes() {
        Navigator.getInstance().showRecipesFragment(FoodActivity.this);
    }
}
