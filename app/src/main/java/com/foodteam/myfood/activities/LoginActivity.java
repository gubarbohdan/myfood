package com.foodteam.myfood.activities;

import android.os.Bundle;

import com.foodteam.myfood.R;
import com.foodteam.myfood.classes.PrefUtils;
import com.foodteam.myfood.utils.Navigator;

public class LoginActivity extends FoodActivity {

    private int exit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        PrefUtils prefUtils = new PrefUtils(getApplicationContext());
        prefUtils.updateAll();

        Navigator.getInstance().showLoginFragment(LoginActivity.this);
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.my_fridge, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
////        if (id == R.id.action_settings) {
////            return true;
////        }
//
//        return super.onOptionsItemSelected(item);
//    }
}
