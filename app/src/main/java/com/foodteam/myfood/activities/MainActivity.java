package com.foodteam.myfood.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.LruCache;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.foodteam.myfood.R;
import com.foodteam.myfood.adapters.MyFridgeAdapter;
import com.foodteam.myfood.classes.PrefUtils;
import com.foodteam.myfood.constants.Locales;
import com.foodteam.myfood.constants.Preferences;
import com.foodteam.myfood.customViews.CircularNetworkImageView;
import com.foodteam.myfood.customViews.ScrimInsetsFrameLayout;
import com.foodteam.myfood.database.DatabaseHelper;
import com.foodteam.myfood.fragments.MyFoodFragment;
import com.foodteam.myfood.managers.ManagerTypeface;
import com.foodteam.myfood.model.Fridge;
import com.foodteam.myfood.utils.Navigator;
import com.foodteam.myfood.utils.UtilsDevice;
import com.foodteam.myfood.utils.UtilsMiscellaneous;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends FoodActivity implements View.OnClickListener {

    private final static double sNAVIGATION_DRAWER_ACCOUNT_SECTION_ASPECT_RATIO = 9d / 16d;
    private DrawerLayout mDrawerLayout;
    private FrameLayout mFrameLayout_AccountView;
    private LinearLayout mNavDrawerEntriesRootView;
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private ScrimInsetsFrameLayout mScrimInsetsFrameLayout;
    private FrameLayout mFrameLayout_Home, mFrameLayout_FridgeList, mFrameLayout_HelpAndFeedback,
            mFrameLayout_About, mFrameLayout_Recipes, mFrameLayout_Setings, mFrameLayout_ShopingList;
    private TextView mTextView_AccountDisplayName, mTextView_AccountEmail;
    private TextView mTextView_Home, mTextView_HelpAndFeedback, mTextView_About;
    private PrefUtils mPrefUtils;
    private CircularNetworkImageView mFacebookCircleImageView;
    private NetworkImageView mFacebookCoverView;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private ListView mListFridge;
    private ImageView mMenuDown;
    private MyFridgeAdapter mAdapter;
    private List<Fridge> mFridgeList;
    private ConnectivityManager cm;
    private MaterialDialog dialog;

    private void getFridgeList() {
        mFridgeList.clear();
        if (mFrameLayout_FridgeList.getVisibility() == View.VISIBLE) {
            mFrameLayout_FridgeList.setVisibility(View.GONE);
            mMenuDown.setImageResource(R.drawable.ic_menu_down_grey600_24dp);
        } else {
            mFrameLayout_FridgeList.setVisibility(View.VISIBLE);
            mMenuDown.setImageResource(R.drawable.ic_menu_up_grey600_24dp);

        }

        DatabaseHelper.getFridgeList(mFridgeList, mAdapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialise();
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkIntent();
    }

    private void checkIntent() {
        if (getIntent().hasExtra(Preferences.EXTRA_REQUEST)) {
            Navigator.getInstance().showAcceptFragment(this, Preferences.TYPE_ACCEPT_INVITE);
        } else if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            Intent loginIntent = new Intent(this, LoginActivity.class);
            startActivityForResult(loginIntent, 1);
        } else {
            Navigator.getInstance().showMyFoodFragment(MainActivity.this);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUserInfo();
    }

    private void setUserInfo() {
        mTextView_AccountDisplayName.setText(mPrefUtils.getUsername());
        mTextView_AccountEmail.setText(mPrefUtils.getEmail());
        mFacebookCircleImageView.setImageUrl(mPrefUtils.getProfileImage(), mImageLoader);
        mFacebookCoverView.setImageUrl(mPrefUtils.getCoverImage(), mImageLoader);
    }

    private void setImageLoader() {
        mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        mImageLoader = new ImageLoader(mRequestQueue, new ImageLoader.ImageCache() {
            private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(10);

            public void putBitmap(String url, Bitmap bitmap) {
                mCache.put(url, bitmap);
            }

            public Bitmap getBitmap(String url) {
                return mCache.get(url);
            }
        });
    }

    /**
     * Bind, create and set up the resources
     */
    private void initialise() {
        // Toolbar
        final Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        mFridgeList = new ArrayList<>();

        mAdapter = new MyFridgeAdapter(this, R.layout.item_fridge,
                mFridgeList);


        mPrefUtils = new PrefUtils(getApplicationContext());
        // Layout resources
        mFrameLayout_AccountView = (FrameLayout) findViewById(R.id.navigation_drawer_account_view);
        mNavDrawerEntriesRootView = (LinearLayout) findViewById(R.id.navigation_drawer_linearLayout_entries_root_view);

        mFrameLayout_Home = (FrameLayout) findViewById(R.id.navigation_drawer_items_list_linearLayout_home);
        mFrameLayout_FridgeList = (FrameLayout) findViewById(R.id.navigation_drawer_list_fridge);
        mFrameLayout_Recipes = (FrameLayout) findViewById(R.id.navigation_drawer_items_list_linearLayout_recipes);

        //ToDo uncoment, when done shoping list
        mFrameLayout_ShopingList = (FrameLayout) findViewById(R.id.navigation_drawer_items_list_linearLayout_shopping);
        mFrameLayout_ShopingList.setVisibility(View.GONE);

        mMenuDown = (ImageView) findViewById(R.id.navigation_drawer_fridge_icon);
        mListFridge = (ListView) findViewById(R.id.fridge_list);

        mFrameLayout_FridgeList.setVisibility(View.GONE);

        //ToDo uncoment, when done settings
        mFrameLayout_Setings = (FrameLayout) findViewById(R.id.navigation_drawer_items_list_linearLayout_settings);
        mFrameLayout_Setings.setVisibility(View.GONE);

        //ToDo uncoment, when done Feedback
        mFrameLayout_HelpAndFeedback = (FrameLayout) findViewById(R.id.navigation_drawer_items_list_linearLayout_feedback);
        mFrameLayout_HelpAndFeedback.setVisibility(View.GONE);

        mFrameLayout_About = (FrameLayout) findViewById(R.id.navigation_drawer_items_list_linearLayout_about);

        mTextView_AccountDisplayName = (TextView) findViewById(R.id.navigation_drawer_account_information_display_name);
        mTextView_AccountEmail = (TextView) findViewById(R.id.navigation_drawer_account_information_email);

        mTextView_Home = (TextView) findViewById(R.id.navigation_drawer_items_textView_home);
        mTextView_HelpAndFeedback = (TextView) findViewById(R.id.navigation_drawer_items_textView_help_and_feedback);
        mTextView_About = (TextView) findViewById(R.id.navigation_drawer_items_textView_about);

        // Typefaces
        mTextView_AccountDisplayName.setTypeface(ManagerTypeface.getTypeface(this, R.string.typeface_roboto_medium));
        mTextView_AccountEmail.setTypeface(ManagerTypeface.getTypeface(this, R.string.typeface_roboto_regular));
        mTextView_Home.setTypeface(ManagerTypeface.getTypeface(this, R.string.typeface_roboto_medium));
        mTextView_HelpAndFeedback.setTypeface(ManagerTypeface.getTypeface(this, R.string.typeface_roboto_medium));
        mTextView_About.setTypeface(ManagerTypeface.getTypeface(this, R.string.typeface_roboto_medium));

        // Navigation Drawer
        mDrawerLayout = (DrawerLayout) findViewById(R.id.main_activity_DrawerLayout);
        mDrawerLayout.setStatusBarBackgroundColor(getResources().getColor(R.color.primaryDark));
        mScrimInsetsFrameLayout = (ScrimInsetsFrameLayout) findViewById(R.id.main_activity_navigation_drawer_rootLayout);
        mFacebookCircleImageView = (CircularNetworkImageView) findViewById(R.id.navigation_drawer_user_account_picture_profile);
        mFacebookCoverView = (NetworkImageView) findViewById(R.id.navigation_drawer_user_account_picture_cover);
        cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        mActionBarDrawerToggle = new ActionBarDrawerToggle
                (
                        this,
                        mDrawerLayout,
                        mToolbar,
                        R.string.navigation_drawer_opened,
                        R.string.navigation_drawer_closed
                ) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                // Disables the burger/arrow animation by default
                super.onDrawerSlide(drawerView, 0);
            }
        };

        mDrawerLayout.setDrawerListener(mActionBarDrawerToggle);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }

        mActionBarDrawerToggle.syncState();

        // Navigation Drawer layout width
        int possibleMinDrawerWidth = UtilsDevice.getScreenWidth(this) -
                UtilsMiscellaneous.getThemeAttributeDimensionSize(this, android.R.attr.actionBarSize);
        int maxDrawerWidth = getResources().getDimensionPixelSize(R.dimen.navigation_drawer_max_width);

        mScrimInsetsFrameLayout.getLayoutParams().width = Math.min(possibleMinDrawerWidth, maxDrawerWidth);

        // Account section height
        mFrameLayout_AccountView.getLayoutParams().height = (int) (mScrimInsetsFrameLayout.getLayoutParams().width
                * sNAVIGATION_DRAWER_ACCOUNT_SECTION_ASPECT_RATIO);

        // Nav Drawer item click listener
        mFrameLayout_AccountView.setOnClickListener(this);
        mFrameLayout_Home.setOnClickListener(this);
        mMenuDown.setOnClickListener(this);
        mFrameLayout_Recipes.setOnClickListener(this);
        //mFrameLayout_Explore.setOnClickListener(this);
        mFrameLayout_HelpAndFeedback.setOnClickListener(this);
        mFrameLayout_About.setOnClickListener(this);

        mPrefUtils.setSupportedLocales();
        getLanguage();


        // Set the first item as selected for the first time
        getSupportActionBar().setTitle(R.string.toolbar_title_myfood);
        mFrameLayout_Home.setSelected(true);

        setImageLoader();

        mFrameLayout_HelpAndFeedback.setVisibility(View.GONE);

        mListFridge.setAdapter(mAdapter);

    }

    private void getLanguage() {
        String lang = getResources().getConfiguration().locale.getLanguage();
        if (mPrefUtils.getSupportedLocales().contains(lang)) {
            mPrefUtils.setLocale(lang);
        } else {
            mPrefUtils.setLocale(Locales.LOCAE_ENGLISH);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.navigation_drawer_fridge_icon:
                if (cm.getActiveNetworkInfo() != null) {
                        getFridgeList();
                } else {
                    Toast.makeText(getApplicationContext(), R.string.msg_no_network, Toast.LENGTH_SHORT).show();
                }
                break;
        }

        if (view.getId() == R.id.navigation_drawer_account_view) {
            mDrawerLayout.closeDrawer(Gravity.LEFT);

            // If the user is signed in, go to the profile, otherwise show sign up / sign in
        } else {
            if (!view.isSelected() && view.getId() != R.id.navigation_drawer_fridge_icon) {
                onRowPressed((FrameLayout) view);

                switch (view.getId()) {
                    case R.id.navigation_drawer_items_list_linearLayout_home: {

                        mFrameLayout_FridgeList.setVisibility(View.GONE);
                        mMenuDown.setImageResource(R.drawable.ic_menu_down_grey600_24dp);
                        if (getSupportActionBar() != null) {
                            getSupportActionBar().setTitle(getString(R.string.toolbar_title_myfood));
                        }

                        view.setSelected(true);

                        MyFoodFragment foodFragment = new MyFoodFragment();
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.main_activity_content_frame, foodFragment)
                                .commit();
                        break;
                    }
                    case R.id.navigation_drawer_items_list_linearLayout_recipes:
                        Navigator.getInstance().showRecipesFragment(MainActivity.this);
                        break;
                    case R.id.navigation_drawer_items_list_linearLayout_shopping:
                        MyFoodFragment foodFragment = new MyFoodFragment();
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.main_activity_content_frame, foodFragment)
                                .commit();
                        break;

                    case R.id.navigation_drawer_items_list_linearLayout_feedback:
                        // Start intent to send an email
                        break;

                    case R.id.navigation_drawer_items_list_linearLayout_about:
                        dialog = new MaterialDialog.Builder(this)
                                .title(R.string.dialog_about_title)
                                .customView(R.layout.material_dialog_about, true)
                                .positiveText(R.string.btn_ok)
                                .show();

                        break;
//                    case R.id.navigation_drawer_fridge_icon:
//
//                        break;
                    default:
                        break;
                }

                //to check
//            } else {
//                mDrawerLayout.closeDrawer(Gravity.LEFT);
            } else {
                if (view.isSelected() && view.getId() != R.id.navigation_drawer_fridge_icon) {
                    mDrawerLayout.closeDrawer(Gravity.LEFT);
                }
            }
        }
    }

    public void drawerUpdate() {
        MyFoodFragment fridgeFragment = new MyFoodFragment();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_activity_content_frame, fridgeFragment)
                .commit();
        onRowPressed(mFrameLayout_Home);
        mFrameLayout_FridgeList.setVisibility(View.GONE);
        mMenuDown.setImageResource(R.drawable.ic_menu_down_grey600_24dp);
    }

    /**
     * Set up the rows when any is pressed
     *
     * @param pressedRow is the pressed row in the drawer
     */
    private void onRowPressed(FrameLayout pressedRow) {
        if (pressedRow.getTag() != getResources().getString(R.string.tag_nav_drawer_special_entry)) {
            for (int i = 0; i < mNavDrawerEntriesRootView.getChildCount(); i++) {
                View currentView = mNavDrawerEntriesRootView.getChildAt(i);

                boolean currentViewIsMainEntry = currentView.getTag() ==
                        getResources().getString(R.string.tag_nav_drawer_main_entry);

                if (currentViewIsMainEntry) {
                    if (currentView == pressedRow) {
                        currentView.setSelected(true);
                    } else {
                        currentView.setSelected(false);
                    }
                }
            }
        }

        mDrawerLayout.closeDrawer(Gravity.LEFT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == 0) {
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mActionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    public void closeFridgeList() {
        mFrameLayout_FridgeList.setVisibility(View.GONE);
    }
}
