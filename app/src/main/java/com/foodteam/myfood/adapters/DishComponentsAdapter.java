package com.foodteam.myfood.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.foodteam.myfood.R;
import com.foodteam.myfood.classes.PrefUtils;
import com.foodteam.myfood.database.DatabaseHelper;
import com.foodteam.myfood.model.Product;
import com.foodteam.myfood.model.ProductItem;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

/**
 * Created by Daryna on 02.12.2015.
 */
public class DishComponentsAdapter extends ArrayAdapter<ProductItem> {
    private Context mContext;
    private int mLayoutResourceId;
    private List<ProductItem> mDishComponent;
    private PrefUtils mPrefUtils;

    public DishComponentsAdapter(Context context, int layoutResourceId, List<ProductItem> dishComponent) {
        super(context, layoutResourceId, dishComponent);
        this.mContext = context;
        this.mLayoutResourceId = layoutResourceId;
        this.mDishComponent = dishComponent;
        mPrefUtils = new PrefUtils(mContext);
        setProducts();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView ==null){
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(mLayoutResourceId, null, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }

        ProductItem dishComponent = getItem(position);


        //TODO load information from parse and set in list view

        holder.tvDishComponentCount.setText(Double.toString(dishComponent.getQuantity()));
        holder.tvDishComponentName.setText(dishComponent.getName());
        holder.tvDishComponentDimension.setText(dishComponent.getMeasure());

        return convertView;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public ProductItem getItem(int position) {
        return mDishComponent.get(position);
    }

    private void setProducts() {
        for (final ProductItem item :
                mDishComponent) {
            DatabaseHelper.getProduct(item.getId(), mPrefUtils.getLocale(), new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Product newItem = dataSnapshot.getValue(Product.class);
                    item.setValues(newItem);
                    notifyDataSetChanged();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    public static class ViewHolder{
        public TextView tvDishComponentCount;
        public TextView tvDishComponentDimension;
        public TextView tvDishComponentName;
        public ViewHolder(View view) {
            tvDishComponentCount = (TextView)view.findViewById(R.id.dish_components_count);
            tvDishComponentDimension = (TextView)view.findViewById(R.id.dish_components_dimension);
            tvDishComponentName = (TextView)view.findViewById(R.id.dish_components_name);
        }

    }
}
