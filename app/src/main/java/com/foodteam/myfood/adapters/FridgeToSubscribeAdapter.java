package com.foodteam.myfood.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.foodteam.myfood.R;
import com.foodteam.myfood.classes.Analytic;
import com.foodteam.myfood.model.Fridge;

import java.util.List;

/**
 * Created by Daryna on 20.10.2015.
 */
public class FridgeToSubscribeAdapter extends ArrayAdapter<Fridge> {
    private Context mContext;
    private int mLayoutResourceId;
    private List<Fridge> mFridge;

    public FridgeToSubscribeAdapter(Context context, int layoutResourceId, List<Fridge> fridge, Analytic analytic) {
        super(context, layoutResourceId, fridge);
        this.mContext = context;
        this.mLayoutResourceId = layoutResourceId;
        this.mFridge = fridge;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(mLayoutResourceId, null, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Fridge currentFridge = getItem(position);

        //TODO load information from parse and set in list view

        holder.tvName.setText(currentFridge.getName());
        holder.tvAuthor.setText(currentFridge.getAuthor());
        holder.ivSubscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.ivSubscribe.setImageResource(R.drawable.ic_checkbox_marked_circle_outline_grey600_36dp);
            }
        });


        return convertView;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Fridge getItem(int position) {
        return mFridge.get(position);
    }

    public static class ViewHolder {
        public ImageView ivSubscribe;
        public TextView tvName, tvAuthor;

        public ViewHolder(View view) {
            ivSubscribe = (ImageView) view.findViewById(R.id.button_subscribe);
            tvName = (TextView) view.findViewById(R.id.fridge_name);
            tvAuthor = (TextView) view.findViewById(R.id.fridge_author_name);
        }
    }
}
