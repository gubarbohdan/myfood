package com.foodteam.myfood.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.foodteam.myfood.R;
import com.foodteam.myfood.activities.MainActivity;
import com.foodteam.myfood.classes.PrefUtils;
import com.foodteam.myfood.model.Fridge;

import java.util.List;

/**
 * Created by Daryna on 05.11.2015.
 */
public class MyFridgeAdapter extends ArrayAdapter<Fridge> {
    private Context mContext;
    private int mLayoutResourceId;
    private List<Fridge> mFridge;
    private PrefUtils prefUtils;

    public MyFridgeAdapter(Context context, int layoutResourceId, List<Fridge> fridge) {
        super(context, layoutResourceId, fridge);
        this.mContext = context;
        this.mLayoutResourceId = layoutResourceId;
        this.mFridge = fridge;
        prefUtils = new PrefUtils(mContext);
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(mLayoutResourceId, null, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Fridge fridgeName = getItem(position);
        holder.tvName.setText(fridgeName.getName());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prefUtils.setCurrentFridge(getItem(position).getId());
                ((MainActivity) mContext).drawerUpdate();
            }
        });
        return convertView;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Fridge getItem(int position) {
        return mFridge.get(position);
    }

    public static class ViewHolder {
        //public ImageView ivFridge;
        public TextView tvName;

        public ViewHolder(View view) {
            //ivFridge = (ImageView) view.findViewById(R.id.icon_fridge);
            tvName = (TextView) view.findViewById(R.id.fridge_name);
        }
    }
}
