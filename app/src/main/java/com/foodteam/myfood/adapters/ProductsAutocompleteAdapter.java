package com.foodteam.myfood.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.foodteam.myfood.R;
import com.foodteam.myfood.classes.PrefUtils;
import com.foodteam.myfood.constants.Node;
import com.foodteam.myfood.model.Product;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gubar on 25.10.2015.
 */
public class ProductsAutocompleteAdapter extends BaseAdapter implements Filterable {


    private final Context mContext;
    private List<Product> mResults;
    private PrefUtils prefUtils;
    private Integer size;

    public ProductsAutocompleteAdapter(Context context) {
        mContext = context;
        mResults = new ArrayList<>();
        prefUtils = new PrefUtils(mContext);
        size = 0;
    }

    @Override
    public int getCount() {
        return mResults.size();
    }

    @Override
    public Product getItem(int position) {
        return mResults.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.autocomplete_layout, parent, false);
        }
        Product product = getItem(position);
            ((TextView) convertView.findViewById(R.id.autocomplete_products)).setText(product.getName());

        return convertView;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    List<Product> books = findProducts(constraint.toString());
                    // Assign the data to the FilterResults
                    filterResults.values = books;
                    filterResults.count = size;
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    mResults = (List<Product>) results.values;
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };

        return filter;
    }

    /**
     * Returns a search result for the given book title.
     */
    private List<Product> findProducts(final String productName) {

        final List<Product> list = new ArrayList<>();

        DatabaseReference productRef = FirebaseDatabase.getInstance().getReference().child(Node.PRODUCTS).child(prefUtils.getLocale());
        productRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()
                        ) {
                    Product product = data.getValue(Product.class);
                    product.setId(data.getKey());
                    if (product.getName().startsWith(productName)) {
                        list.add(product);
                        size++;
                    }
                }
                //notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return list;
    }
}
