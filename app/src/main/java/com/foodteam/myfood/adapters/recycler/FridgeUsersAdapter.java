package com.foodteam.myfood.adapters.recycler;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.foodteam.myfood.R;
import com.foodteam.myfood.classes.Analytic;
import com.foodteam.myfood.classes.PrefUtils;
import com.foodteam.myfood.customViews.CircularNetworkImageView;
import com.foodteam.myfood.database.DatabaseHelper;
import com.foodteam.myfood.model.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

/**
 * Created by gubar on 10.02.2017.
 */

public class FridgeUsersAdapter extends RecyclerView.Adapter<FridgeUsersAdapter.ViewHolder> {

    private Context mContext;
    private List<User> mUser;
    private User currentUser;
    private ImageLoader mImageLoader;
    private RequestQueue mRequestQueue;
    private PrefUtils mPrefUtils;
    private Analytic mAnalytic;

    public FridgeUsersAdapter(Context context, List<User> users, Analytic analytic) {
        this.mContext = context;
        this.mUser = users;
        this.mAnalytic = analytic;
        mPrefUtils = new PrefUtils(mContext);
        setImageLoader();
    }

    private void setImageLoader() {
        mRequestQueue = Volley.newRequestQueue(mContext);
        mImageLoader = new ImageLoader(mRequestQueue, new ImageLoader.ImageCache() {
            private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(10);

            public void putBitmap(String url, Bitmap bitmap) {
                mCache.put(url, bitmap);
            }

            public Bitmap getBitmap(String url) {
                return mCache.get(url);
            }
        });
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_fridge_users, parent, false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        currentUser = getItem(position);

        //TODO load information from parse and set in list view

        holder.ivPhoto.setImageUrl(currentUser.getPhoto(), mImageLoader);
        holder.tvName.setText(currentUser.getName());
        holder.bDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseHelper.getFridgeName(mPrefUtils.getCurrentFridgeId(), new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String msg = String.format(mContext.getResources().getString(R.string.dialog_delete_users_msg), getItem(position).getName(), dataSnapshot.getValue(String.class));
                        MaterialDialog deleteUsersDialog = new MaterialDialog.Builder(mContext)
                                .title(R.string.dialog_delete_users_title)
                                .positiveText(R.string.btn_ok)
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                                        DatabaseHelper.deleteFromFridge(mPrefUtils.getCurrentFridgeId(), getItem(position).getId());
                                        mUser.remove(getItem(position));
                                        notifyDataSetChanged();
                                        mAnalytic.sendEvent("Users", "Delete from fridge");
                                    }
                                })
                                .negativeText(R.string.btn_cancel)
                                .content(msg)
                                .show();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }
        });
    }

    public User getItem(int position) {
        return mUser.get(position);
    }

    @Override
    public int getItemCount() {
        return mUser.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public CircularNetworkImageView ivPhoto;
        public TextView tvName;
        public ImageView bDelete;

        public ViewHolder(View itemView) {
            super(itemView);
            ivPhoto = (CircularNetworkImageView) itemView.findViewById(R.id.friend_photo);
            bDelete = (ImageView) itemView.findViewById(R.id.deleteUserButton);
            tvName = (TextView) itemView.findViewById(R.id.friend_name);
        }
    }
}
