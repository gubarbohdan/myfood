package com.foodteam.myfood.adapters.recycler;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.foodteam.myfood.R;
import com.foodteam.myfood.classes.Analytic;
import com.foodteam.myfood.database.DatabaseHelper;
import com.foodteam.myfood.model.Fridge;
import com.foodteam.myfood.model.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

/**
 * Created by gubar on 10.02.2017.
 */

public class FriendsToAcceptAdapter extends RecyclerView.Adapter<FriendsToAcceptAdapter.ViewHolder> {

    private List<Fridge> mUser;
    private Analytic analytic;

    public FriendsToAcceptAdapter(List<Fridge> user, Analytic analytic) {
        this.mUser = user;
        this.analytic = analytic;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_accept, parent, false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Fridge currentItem = getItem(position);
        DatabaseHelper.getUser(currentItem.getAuthor(), new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                holder.tvFriendName.setText(user.getName());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //TODO load information from parse and set in list view

        holder.tvFridgeName.setText(currentItem.getName());


        holder.ivAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseHelper.acceptInvite(currentItem);
                mUser.remove(currentItem);
                analytic.sendEvent("Users", "Accept");
                notifyDataSetChanged();
            }
        });
        holder.ivReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseHelper.declineInvite(currentItem);
                mUser.remove(getItem(position));
                analytic.sendEvent("Users", "Deny");
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mUser.size();
    }

    public Fridge getItem(int position) {
        return mUser.get(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView ivReject, ivAccept;
        public TextView tvFridgeName, tvFriendName;

        public ViewHolder(View itemView) {
            super(itemView);
            tvFriendName = (TextView) itemView.findViewById(R.id.fridge_name);
            ivReject = (ImageView) itemView.findViewById(R.id.friend_reject);
            ivAccept = (ImageView) itemView.findViewById(R.id.friend_accept);
            tvFridgeName = (TextView) itemView.findViewById(R.id.friend_name);
        }
    }
}
