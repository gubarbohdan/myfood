package com.foodteam.myfood.adapters.recycler;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.foodteam.myfood.R;
import com.foodteam.myfood.customViews.CircularNetworkImageView;
import com.foodteam.myfood.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gubar on 10.02.2017.
 */

public class FriendsToInviteAdapter extends RecyclerView.Adapter<FriendsToInviteAdapter.ViewHolder> {

    private final List<User> list = new ArrayList<User>();
    private Context mContext;
    private List<User> mUser;
    private User mCurrentUser;
    private ImageLoader mImageLoader;
    private RequestQueue mRequestQueue;

    public FriendsToInviteAdapter(Context context, List<User> user) {
        this.mContext = context;
        this.mUser = user;
        setImageLoader();
    }

    private void setImageLoader() {
        mRequestQueue = Volley.newRequestQueue(mContext);
        mImageLoader = new ImageLoader(mRequestQueue, new ImageLoader.ImageCache() {
            private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(10);

            public void putBitmap(String url, Bitmap bitmap) {
                mCache.put(url, bitmap);
            }

            public Bitmap getBitmap(String url) {
                return mCache.get(url);
            }
        });
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_friends_to_invite, parent, false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        mCurrentUser = getItem(position);

        //TODO load information from parse and set in list view

        holder.ivPhoto.setImageUrl(mCurrentUser.getPhoto(), mImageLoader);
        holder.tvName.setText(mCurrentUser.getName());
        holder.cbInvite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    list.add(getItem(position));
                } else {
                    list.remove(getItem(position));
                }
            }
        });
    }

    public User getItem(int position) {
        return mUser.get(position);
    }

    @Override
    public int getItemCount() {
        return mUser.size();
    }

    public List<User> getCheckedList() {
        return list;
    }

    public List<User> getFriendsList() {
        return mUser;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public CircularNetworkImageView ivPhoto;
        public TextView tvName;
        public CheckBox cbInvite;

        public ViewHolder(View itemView) {
            super(itemView);
            ivPhoto = (CircularNetworkImageView) itemView.findViewById(R.id.friend_photo);
            cbInvite = (CheckBox) itemView.findViewById(R.id.friend_to_invite);
            tvName = (TextView) itemView.findViewById(R.id.friend_name);
        }
    }
}
