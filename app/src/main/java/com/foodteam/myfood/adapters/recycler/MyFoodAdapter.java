package com.foodteam.myfood.adapters.recycler;

import android.content.Context;
import android.net.ConnectivityManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.foodteam.myfood.R;
import com.foodteam.myfood.classes.Analytic;
import com.foodteam.myfood.classes.FirebaseImageLoader;
import com.foodteam.myfood.classes.PrefUtils;
import com.foodteam.myfood.constants.Node;
import com.foodteam.myfood.database.DatabaseHelper;
import com.foodteam.myfood.model.ProductItem;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.StorageReference;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;

/**
 * Created by gubar on 10.02.2017.
 */

public class MyFoodAdapter extends RecyclerView.Adapter<MyFoodAdapter.ViewHolder> {

    protected MaterialDialog dialog;
    Comparator<ProductItem> comp = new Comparator<ProductItem>() {
        @Override
        public int compare(ProductItem lhs, ProductItem rhs) {
            return lhs.getQuantity() > rhs.getQuantity() ? -1 : 1;
        }
    };
    private Context mContext;
    private List<ProductItem> mFridgeItem;
    private ConnectivityManager cm;
    private SeekBar mSeekBar;
    private double gram;
    private PrefUtils mPrefUtils;
    private Analytic mAnalytic;

    public MyFoodAdapter(Context context, List<ProductItem> fridgeItems, Analytic mAnalytic) {
        this.mContext = context;
        this.mFridgeItem = fridgeItems;
        this.mAnalytic = mAnalytic;
        cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        mPrefUtils = new PrefUtils(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        ProductItem fridgeItems = getItem(position);

        StorageReference ref = DatabaseHelper.getProductImageRef(fridgeItems.getPhoto());
        Glide.with(mContext).using(new FirebaseImageLoader()).load(ref).centerCrop().placeholder(R.drawable.ic_cart_outline_grey600_24dp).into(holder.ivIcon);

        holder.tvName.setText(mFridgeItem.get(position).getName());
        holder.tvDimension.setText(mFridgeItem.get(position).getMeasure());

        holder.tvCount.setText(String.valueOf(new BigDecimal(fridgeItems.getQuantity()).setScale(2, BigDecimal.ROUND_UP).doubleValue()));
        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cm.getActiveNetworkInfo() != null) {
                    switch (v.getId()) {
                        case R.id.delete_product_icon: {
                            startDeleteDialog(position);
                            break;
                        }
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mFridgeItem.size();
    }

    public ProductItem getItem(int position) {
        return mFridgeItem.get(position);
    }

    private void startDeleteDialog(final int position) {
        String productLocaleName;
        productLocaleName = mFridgeItem.get(position).getName();
        dialog = new MaterialDialog.Builder(mContext)
                .title(mContext.getString(R.string.dialog_delete_products) + " " + productLocaleName)
                .customView(R.layout.material_dialog_delete, true)
                .positiveText(R.string.btn_save)
                .negativeText(R.string.btn_cancel)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                        deleteItem(position, mSeekBar.getProgress());
                    }
                })
                .show();
        mSeekBar = (SeekBar) dialog.findViewById(R.id.seekBar);
        final TextView deleteCount = (TextView) dialog.findViewById(R.id.delete_count);
        final TextView deletePercent = (TextView) dialog.findViewById(R.id.delete_percent);

        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                deletePercent.setText(Integer.toString(progress) + "%");
                double i = getItem(position).getQuantity();
                gram = i / 100 * progress;
                BigDecimal bigDecimal = new BigDecimal(gram);
                gram = bigDecimal.setScale(2, RoundingMode.HALF_UP).doubleValue();
                deleteCount.setText(Double.toString(gram) + " " + mFridgeItem.get(position).getMeasure());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void deleteItem(final int position, final int percent) {
        DatabaseReference delRef = FirebaseDatabase.getInstance().getReference().child(Node.FRIDGES).child(mPrefUtils.getCurrentFridgeId()).child(Node.PRODUCTS);
        if (percent == 100) {
            delRef.child(getItem(position).getId()).removeValue();
        } else if (percent != 0) {
            double quantity = getItem(position).getQuantity() - gram;
            delRef.child(getItem(position).getId()).child(Node.QUANTITY).setValue(quantity);
        }
        mAnalytic.sendEvent("Fridge", "Delete item");
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView ivIcon;
        public TextView tvName;
        public TextView tvCount;
        public TextView tvDimension;
        public ImageView ivDelete;

        public ViewHolder(View itemView) {
            super(itemView);
            ivIcon = (ImageView) itemView.findViewById(R.id.icon_food);
            tvName = (TextView) itemView.findViewById(R.id.food_name);
            tvCount = (TextView) itemView.findViewById(R.id.food_count);
            tvDimension = (TextView) itemView.findViewById(R.id.food_dimension);
            ivDelete = (ImageView) itemView.findViewById(R.id.delete_product_icon);
        }
    }
}
