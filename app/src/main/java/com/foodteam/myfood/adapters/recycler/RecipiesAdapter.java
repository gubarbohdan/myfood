package com.foodteam.myfood.adapters.recycler;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.foodteam.myfood.R;
import com.foodteam.myfood.classes.FirebaseImageLoader;
import com.foodteam.myfood.database.DatabaseHelper;
import com.foodteam.myfood.model.Recipe;
import com.google.firebase.storage.StorageReference;

import java.util.List;

/**
 * Created by gubar on 10.02.2017.
 */

public class RecipiesAdapter extends RecyclerView.Adapter<RecipiesAdapter.ViewHolder> {

    private OnItemClickListener mOnItemClickListener;
    private Context mContext;
    private List<Recipe> mUser;

    public RecipiesAdapter(Context context, List<Recipe> recipes, OnItemClickListener listener) {
        this.mContext = context;
        this.mUser = recipes;
        mOnItemClickListener = listener;
    }

    public Recipe getItem(int position) {
        return mUser.get(position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recipe, parent, false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Recipe currentRecipe = getItem(position);

        holder.tvName.setText(currentRecipe.getName());
        StorageReference ref = DatabaseHelper.getRecipeImageRef(currentRecipe.getPhoto());
        Glide.with(mContext).using(new FirebaseImageLoader()).load(ref).into(holder.ivIcon);

        holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mUser.size();
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivIcon;
        public TextView tvName;
        private CardView mView;

        public ViewHolder(View itemView) {
            super(itemView);
            mView = (CardView) itemView.findViewById(R.id.recipes_card_view);
            ivIcon = (ImageView) itemView.findViewById(R.id.recipes_icon);
            tvName = (TextView) itemView.findViewById(R.id.recipeNameTextView);
        }

        public void setOnClickListener(View.OnClickListener listener) {
            mView.setOnClickListener(listener);
        }
    }
}
