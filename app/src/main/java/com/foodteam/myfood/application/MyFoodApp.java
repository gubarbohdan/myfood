package com.foodteam.myfood.application;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.foodteam.myfood.classes.Analytic;

/**
 * Created by gubar on 30.09.2015.
 */
public class MyFoodApp extends Application {
    private Analytic analytic;

    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(getApplicationContext());

    }


    synchronized public Analytic getAnalytic() {
        if (analytic == null) {
            analytic = new Analytic(getApplicationContext());
        }
        return analytic;
    }
}
