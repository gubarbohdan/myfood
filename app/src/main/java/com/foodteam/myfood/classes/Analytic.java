package com.foodteam.myfood.classes;

import android.content.Context;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * Created by gubar on 14.01.2016.
 */
public class Analytic {
    private FirebaseAnalytics mFirebaseAnalytics;

    public Analytic(Context context) {
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
    }

    synchronized public void sendView(Class c) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, c.getCanonicalName());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, c.getCanonicalName());
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }

    synchronized public void sendEvent(String category, String action) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, category);
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, action);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }
}
