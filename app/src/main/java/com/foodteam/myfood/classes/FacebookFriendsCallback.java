package com.foodteam.myfood.classes;

import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.foodteam.myfood.constants.Node;
import com.foodteam.myfood.model.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gubar on 12.10.2015.
 */
public class FacebookFriendsCallback implements GraphRequest.Callback {
    private List<User> friendUsers;

    @Override
    public void onCompleted(GraphResponse response) {
        //ToDo make users request
        JSONObject friendsRequest = response.getJSONObject();
        friendUsers = new ArrayList<>();
        try {
            JSONArray friendsJSONArray = friendsRequest.getJSONArray("data");
            final List<String> friendsList = new ArrayList<String>();
            for (int i = 0; i < friendsJSONArray.length(); i++) {
                friendsList.add(friendsJSONArray.getJSONObject(i).getString("id"));
            }

            final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(Node.USERS);
            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        User user = data.getValue(User.class);
                        if (friendsList.contains(user.getFacebookId())) {
                            friendUsers.add(user);
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (JSONException ex) {

        }
    }

    public List<User> getFriendUsers() {
        return friendUsers;
    }
}
