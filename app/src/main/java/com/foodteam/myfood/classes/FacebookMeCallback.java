package com.foodteam.myfood.classes;

import android.net.Uri;
import android.util.Log;

import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.internal.ImageRequest;
import com.foodteam.myfood.constants.FacebookConstants;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by gubar on 12.10.2015.
 */
public class FacebookMeCallback implements GraphRequest.GraphJSONObjectCallback {
    private PrefUtils prefUtils;

    public FacebookMeCallback(PrefUtils sharedPreferences) {
        this.prefUtils = sharedPreferences;
    }

    @Override
    public void onCompleted(JSONObject object, GraphResponse response) {
        try {
            Log.d("TAG1", object.toString());
            prefUtils.setFacebookId(object.getString(FacebookConstants.FACEBOOK_RESPONSE_ID));
            prefUtils.setUsername(object.getString(FacebookConstants.FACEBOOK_RESPONSE_NAME));
            if (object.has(FacebookConstants.FACEBOOK_RESPONSE_EMAIL)) {
                prefUtils.setEmail(object.getString(FacebookConstants.FACEBOOK_RESPONSE_EMAIL));
            }

            Uri uri = ImageRequest.getProfilePictureUri(
                    object.optString(FacebookConstants.FACEBOOK_RESPONSE_ID), 999, 999);
            prefUtils.setProfileImage(uri.toString());

            if (object.has(FacebookConstants.FACEBOOK_RESPONSE_COVER)) {
                prefUtils.setCoverImage(object.getJSONObject(FacebookConstants.FACEBOOK_RESPONSE_COVER).getString(FacebookConstants.FACEBOOK_RESPONSE_SOURCE));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
