package com.foodteam.myfood.classes;

import android.content.Context;
import android.content.SharedPreferences;

import com.foodteam.myfood.constants.FacebookConstants;
import com.foodteam.myfood.constants.GoogleConstants;
import com.foodteam.myfood.constants.Preferences;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by gubar on 03.11.2015.
 */
public class PrefUtils {
    public static final String PREF_NAME = "prefs";
    public static final String UPDATE_PRODUCTS = "update_products";
    public static final String UPDATE_FRIDGES = "update_fridges";
    public static final String UPDATE_FRIDGE_ITEM = "update_item";
    public static final String SEND_PRODUCTS = "send_products";
    public static final String UPDATE_CATEGORIES = "update_categories";
    public static final String UPDATE_RECIPES = "update_recipes";


    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;

    public PrefUtils(Context context) {
        mSharedPreferences = context.getSharedPreferences(Preferences.PREFERENCES, Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
    }

    public String getLocale() {
        return mSharedPreferences.getString(Preferences.LOCALE, "en");
    }

    public void setLocale(String lang) {
        mEditor.putString(Preferences.LOCALE, lang);
        mEditor.commit();
    }

    public void setSupportedLocales() {
        Set<String> localesSet = new HashSet<>();
        localesSet.add("ru");
        localesSet.add("uk");
        mEditor.putStringSet(Preferences.SUPPORTED_LOCALES, localesSet);
        mEditor.commit();
    }

    public Set<String> getSupportedLocales() {
        return mSharedPreferences.getStringSet(Preferences.SUPPORTED_LOCALES, null);
    }

    public boolean needToSendProducts() {
        return mSharedPreferences.getBoolean(SEND_PRODUCTS, false);
    }

    public boolean needToUpdateProducts() {
        return mSharedPreferences.getBoolean(UPDATE_PRODUCTS, true);
    }

    public boolean needToUpdateFridges() {
        return mSharedPreferences.getBoolean(UPDATE_FRIDGES, true);
    }

    public boolean needToUpdateFridgesItem() {
        return mSharedPreferences.getBoolean(UPDATE_FRIDGE_ITEM, true);
    }

    public boolean needToUpdateCategories() {
        return mSharedPreferences.getBoolean(UPDATE_CATEGORIES, true);
    }

    public boolean needToUpdateRecipes() {
        return mSharedPreferences.getBoolean(UPDATE_RECIPES, true);
    }

    public void updateAll() {
        mEditor.putBoolean(UPDATE_PRODUCTS, true);
        mEditor.putBoolean(UPDATE_FRIDGES, true);
        mEditor.putBoolean(UPDATE_FRIDGE_ITEM, true);
        mEditor.putBoolean(UPDATE_RECIPES, true);
        mEditor.putBoolean(UPDATE_CATEGORIES, true);
        mEditor.commit();
    }

    public void updateFridgeItem() {
        mEditor.putBoolean(UPDATE_FRIDGE_ITEM, true);
        mEditor.commit();
    }

    public void updateFridges() {
        mEditor.putBoolean(UPDATE_FRIDGES, true);
        mEditor.commit();
    }

    public void updateRecipes() {
        mEditor.putBoolean(UPDATE_RECIPES, true);
        mEditor.commit();
    }

    public void sendProducts() {
        mEditor.putBoolean(SEND_PRODUCTS, true);
        mEditor.commit();
    }

    public void setProductsUpdated() {
        mEditor.putBoolean(UPDATE_PRODUCTS, false);
        mEditor.commit();
    }

    public void setProductsSended() {
        mEditor.putBoolean(SEND_PRODUCTS, false);
        mEditor.commit();
    }

    public void setFridgesUpdated() {
        mEditor.putBoolean(UPDATE_FRIDGES, false);
        mEditor.commit();
    }

    public void setFridgesItemUpdated() {
        mEditor.putBoolean(UPDATE_FRIDGE_ITEM, false);
        mEditor.commit();
    }

    public void setCategoriesUpdated() {
        mEditor.putBoolean(UPDATE_CATEGORIES, false);
        mEditor.commit();
    }

    public void setRecipesUpdated() {
        mEditor.putBoolean(UPDATE_RECIPES, false);
        mEditor.commit();
    }

    public String getUsername() {
        return mSharedPreferences.getString(Preferences.USERNAME, "Scott");
    }

    public void setUsername(String username) {
        mEditor.putString(Preferences.USERNAME, username);
        mEditor.commit();
    }

    public String getEmail() {
        return mSharedPreferences.getString(Preferences.EMAIL, "test@test.com");
    }

    public void setEmail(String email) {
        mEditor.putString(Preferences.EMAIL, email);
        mEditor.commit();
    }

    public String getProfileImage() {
        return mSharedPreferences.getString(Preferences.PROFILE_IMAGE, null);
    }

    public void setProfileImage(String image) {
        mEditor.putString(Preferences.PROFILE_IMAGE, image);
        mEditor.commit();
    }

    public String getCoverImage() {
        return mSharedPreferences.getString(Preferences.COVER_IMAGE, null);
    }

    public void setCoverImage(String coverImage) {
        mEditor.putString(Preferences.COVER_IMAGE, coverImage);
        mEditor.commit();
    }

    public String getFacebookId() {
        return mSharedPreferences.getString(FacebookConstants.FACEBOOK_ID, null);
    }

    public void setFacebookId(String id) {
        mEditor.putString(FacebookConstants.FACEBOOK_ID, id);
        mEditor.commit();
    }

    public String getGoogleId() {
        return mSharedPreferences.getString(GoogleConstants.GOOGLE_ID, null);
    }

    public void setGoogleId(String id) {
        mEditor.putString(GoogleConstants.GOOGLE_ID, id);
        mEditor.commit();
    }

    public void setCurrentFridge(String fridgeId) {
        mEditor.putString(Preferences.CURRENT_FRIDGE, fridgeId);
        mEditor.commit();
    }

    public String getCurrentFridgeId() {
        return mSharedPreferences.getString(Preferences.CURRENT_FRIDGE, null);
    }

    public void unsubscribeFromFridge() {
        mEditor.remove(Preferences.CURRENT_FRIDGE);
        mEditor.commit();
        updateFridges();
    }
}
