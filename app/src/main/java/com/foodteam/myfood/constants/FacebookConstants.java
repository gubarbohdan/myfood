package com.foodteam.myfood.constants;

/**
 * Created by gubar on 03.02.2017.
 */

public class FacebookConstants {
    public static final String FACEBOOK_ID = "facebook_id";
    public static final String FACEBOOK_PERMISION_EMAIL = "email";
    public static final String FACEBOOK_PERMISION_USER_FRIENDS = "user_friends";
    public static final String FACEBOOK_PERMISION_PUBLIC_PROFILE = "public_profile";
    public static final String FACEBOOK_RESPONSE_ID = "id";
    public static final String FACEBOOK_RESPONSE_NAME = "name";
    public static final String FACEBOOK_RESPONSE_EMAIL = "email";
    public static final String FACEBOOK_RESPONSE_COVER = "cover";
    public static final String FACEBOOK_RESPONSE_SOURCE = "source";
    public static final String FACEBOOK_RESPONSE_DATA = "data";

    public static final String FACEBOOK_REQUEST_FRIENDS = "/me/friends";


}
