package com.foodteam.myfood.constants;

/**
 * Created by gubar on 03.02.2017.
 */

public class Node {
    public static final String USERS = "users";
    public static final String FRIDGES = "fridges";
    public static final String PRODUCTS = "products";
    public static final String RECIPES = "recipes";
    public static final String NAME = "name";
    public static final String PHOTO = "photo";
    public static final String FCM_TOKEN = "fcmToken";
    public static final String QUANTITY = "quantity";
    public static final String ACCEPTED = "accepted";

}
