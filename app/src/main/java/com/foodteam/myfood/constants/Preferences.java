package com.foodteam.myfood.constants;

/**
 * Created by gubar on 03.02.2017.
 */

public class Preferences {
    public static final String PREFERENCES = "prefs";
    public static final String USERNAME = "username";
    public static final String EMAIL = "email";
    public static final String PROFILE_IMAGE = "profile_image";
    public static final String COVER_IMAGE = "cover_image";
    public static final String LOCALE = "locale";
    public static final String SUPPORTED_LOCALES = "supported_locales";
    public static final String CURRENT_FRIDGE = "current_fridge";

    public static final String PUSH_TYPE = "push_type";
    public static final String TYPE_ACCEPT_INVITE = "accept_invite";
    public static final String TYPE_ACCEPT_REQUEST = "accept_request";

    public static final String EXTRA_REQUEST = "openRequests";
}
