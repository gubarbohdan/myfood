package com.foodteam.myfood.constants;

/**
 * Created by gubar on 03.02.2017.
 */

public class Provider {
    public static final String FACEBOOK_PROVIDER = "facebookId";
    public static final String GOOGLE_PROVIDER = "googleId";
}
