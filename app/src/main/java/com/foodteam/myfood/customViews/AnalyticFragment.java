package com.foodteam.myfood.customViews;

import android.support.v4.app.Fragment;

import com.foodteam.myfood.application.MyFoodApp;
import com.foodteam.myfood.classes.Analytic;

/**
 * Created by gubar on 14.01.2016.
 */
public class AnalyticFragment extends Fragment {
    protected Analytic getAnalytic() {
        return ((MyFoodApp) getActivity().getApplication()).getAnalytic();
    }

    @Override
    public void onResume() {
        super.onResume();
        Analytic analytic = getAnalytic();
        analytic.sendView(this.getClass());
    }
}
