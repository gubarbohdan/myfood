package com.foodteam.myfood.customViews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

public class ExtendedListView extends ListView {
    public ExtendedListView(Context context) {
        super(context);
    }

    public ExtendedListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ExtendedListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(MeasureSpec.UNSPECIFIED, 0));

        int childHeight = getMeasuredHeight() - (getListPaddingTop() +
                getListPaddingBottom() + getVerticalFadingEdgeLength() * 2);

        int fullHeight = getListPaddingTop() + getListPaddingBottom() + childHeight * (getCount());

        setMeasuredDimension(getMeasuredWidth(), fullHeight + getCount());
    }

}
