package com.foodteam.myfood.database;

import android.content.Context;
import android.widget.ArrayAdapter;

import com.foodteam.myfood.R;
import com.foodteam.myfood.constants.MessageInfo;
import com.foodteam.myfood.constants.Node;
import com.foodteam.myfood.listeners.GetFridgesListListener;
import com.foodteam.myfood.model.Fridge;
import com.foodteam.myfood.model.User;
import com.foodteam.myfood.model.messages.Message;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by gubar on 03.02.2017.
 */

public class DatabaseHelper {
    private static DatabaseReference sDatabaseReference = FirebaseDatabase.getInstance().getReference();

    public static void createUser(String provider) {
        for (UserInfo info : FirebaseAuth.getInstance().getCurrentUser().getProviderData()) {
            sDatabaseReference.child(Node.USERS).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(provider).setValue(info.getUid());
            sDatabaseReference.child(Node.USERS).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Node.NAME).setValue(info.getDisplayName());
            sDatabaseReference.child(Node.USERS).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Node.PHOTO).setValue(info.getPhotoUrl().toString());
        }
        setFcmToken();
    }

    public static void setFcmToken() {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            sDatabaseReference.child(Node.USERS).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Node.FCM_TOKEN).setValue(FirebaseInstanceId.getInstance().getToken());
        }
    }

    public static void getFridgeList(List<Fridge> fridges, ArrayAdapter<Fridge> adapter) {
        DatabaseReference userRef = sDatabaseReference.child(Node.USERS).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Node.FRIDGES);
        userRef.addChildEventListener(new GetFridgesListListener(fridges, adapter));
    }

    public static String createFridge(Context context, String name, List<User> usersToInvite) {
        Fridge newFridge = new Fridge();
        newFridge.setAuthor(FirebaseAuth.getInstance().getCurrentUser().getUid());
        newFridge.setName(name);
        newFridge.setAccepted(true);
        newFridge.setProducts(null);


        String id = sDatabaseReference.child(Node.FRIDGES).push().getKey();

        sDatabaseReference.child(Node.FRIDGES).child(id).setValue(newFridge);
        sDatabaseReference.child(Node.USERS).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Node.FRIDGES).child(id).setValue(newFridge);
        sDatabaseReference.child(Node.FRIDGES).child(id).child(Node.USERS).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Node.ACCEPTED).setValue(true);

        newFridge.setId(id);

        sendFcmMessage(context, usersToInvite, newFridge);

        return id;
    }

    public static void getFridge(String fridgeId, ValueEventListener listener) {
        sDatabaseReference.child(Node.FRIDGES).child(fridgeId).addListenerForSingleValueEvent(listener);
    }

    public static void sendFcmMessage(Context context, List<User> list, Fridge fridge) {
        String id = fridge.getId();
        fridge.setId(null);
        fridge.setAccepted(false);

        for (User u :
                list) {
            sDatabaseReference.child(Node.USERS).child(u.getId()).child(Node.FRIDGES).child(id).setValue(fridge);
            sDatabaseReference.child(Node.FRIDGES).child(id).child(Node.USERS).child(u.getId()).child(Node.ACCEPTED).setValue(false);
        }

        String text = String.format(context.getResources().getString(R.string.notification_invite), FirebaseAuth.getInstance().getCurrentUser().getDisplayName(), fridge.getName());

        
        Gson gson = new Gson();
        String jsonString = gson.toJson(new Message(context, list, text));

        MediaType type = MediaType.parse(MessageInfo.TYPE_JSON);
        OkHttpClient client = new OkHttpClient();
        RequestBody body = RequestBody.create(type, jsonString);

        Request request = new Request.Builder().url(MessageInfo.URL).addHeader(MessageInfo.HEADER_CONTENT_TYPE, MessageInfo.TYPE_JSON).addHeader(MessageInfo.HEADER_AUTHORIZATION, MessageInfo.AUTHORIZATION_KEY)
                .post(body)
                .build();


        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                response.body().toString();
            }
        });
    }

    public static void getInvitesToAccept(ValueEventListener listener) {
        sDatabaseReference.child(Node.USERS).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Node.FRIDGES).addListenerForSingleValueEvent(listener);
    }

    public static void acceptInvite(Fridge fridge) {
        sDatabaseReference.child(Node.USERS).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Node.FRIDGES).child(fridge.getId()).child(Node.ACCEPTED).setValue(true);
        sDatabaseReference.child(Node.FRIDGES).child(fridge.getId()).child(Node.USERS).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Node.ACCEPTED).setValue(true);

    }

    public static void declineInvite(Fridge fridge) {
        sDatabaseReference.child(Node.USERS).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Node.FRIDGES).child(fridge.getId()).removeValue();
    }

    public static void getUser(String userId, ValueEventListener listener) {
        sDatabaseReference.child(Node.USERS).child(userId).addListenerForSingleValueEvent(listener);
    }

    public static void deleteFridge(final String fridgeId) {
        sDatabaseReference.child(Node.FRIDGES).child(fridgeId).child(Node.USERS).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                long count = dataSnapshot.getChildrenCount();
                int i = 0;
                for (DataSnapshot data : dataSnapshot.getChildren()
                        ) {
                    sDatabaseReference.child(Node.USERS).child(data.getKey()).child(Node.FRIDGES).child(fridgeId).removeValue();
                    i++;
                    if (i == count) {
                        sDatabaseReference.child(Node.FRIDGES).child(fridgeId).removeValue();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public static void unsubscribeFromFridge(String fridgeId) {
        sDatabaseReference.child(Node.USERS).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Node.FRIDGES).child(fridgeId).removeValue();
        sDatabaseReference.child(Node.FRIDGES).child(fridgeId).child(Node.USERS).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).removeValue();
    }

    public static void getCategories(String locale, ValueEventListener listener) {
        sDatabaseReference.child(Node.RECIPES).child(locale).addListenerForSingleValueEvent(listener);
    }

    public static void getRecipesByCategory(String category, String locale, ValueEventListener listener) {
        sDatabaseReference.child(Node.RECIPES).child(locale).child(category).addListenerForSingleValueEvent(listener);
    }

    public static void getRecipeById(String category, String id, String locale, ValueEventListener listener, ValueEventListener productListener) {
        sDatabaseReference.child(Node.RECIPES).child(locale).child(category).child(id).addListenerForSingleValueEvent(listener);
        sDatabaseReference.child(Node.RECIPES).child(locale).child(category).child(id).child(Node.PRODUCTS).addListenerForSingleValueEvent(productListener);
    }

    public static void getProduct(String id, String locale, ValueEventListener listener) {
        sDatabaseReference.child(Node.PRODUCTS).child(locale).child(id).addListenerForSingleValueEvent(listener);
    }

    public static void getUserList(String fridgeId, ValueEventListener listener) {
        sDatabaseReference.child(Node.FRIDGES).child(fridgeId).child(Node.USERS).addListenerForSingleValueEvent(listener);
    }

    public static void deleteFromFridge(String fridgeId, String userId) {
        sDatabaseReference.child(Node.USERS).child(userId).child(Node.FRIDGES).child(fridgeId).removeValue();
        sDatabaseReference.child(Node.FRIDGES).child(fridgeId).child(Node.USERS).child(userId).removeValue();
    }

    public static void getFridgeName(String fridgeId, ValueEventListener listener) {
        sDatabaseReference.child(Node.FRIDGES).child(fridgeId).child(Node.NAME).addListenerForSingleValueEvent(listener);
    }

    public static StorageReference getProductImageRef(String image) {
        StorageReference ref = FirebaseStorage.getInstance().getReferenceFromUrl("gs://project-7763933523000828500.appspot.com");
        StorageReference imageRef = ref.child("images");
        StorageReference productsRef = imageRef.child("products");
        StorageReference itemRef = productsRef.child(image);
        return itemRef;
    }

    public static StorageReference getRecipeImageRef(String image) {
        StorageReference ref = FirebaseStorage.getInstance().getReferenceFromUrl("gs://project-7763933523000828500.appspot.com");
        StorageReference imageRef = ref.child("images");
        StorageReference recipes = imageRef.child("recipes");
        StorageReference itemRef = recipes.child(image);
        return itemRef;
    }
}
