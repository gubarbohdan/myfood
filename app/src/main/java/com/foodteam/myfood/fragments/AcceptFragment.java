package com.foodteam.myfood.fragments;


import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.foodteam.myfood.R;
import com.foodteam.myfood.adapters.recycler.FriendsToAcceptAdapter;
import com.foodteam.myfood.classes.Analytic;
import com.foodteam.myfood.classes.EmptyRecyclerView;
import com.foodteam.myfood.constants.Preferences;
import com.foodteam.myfood.customViews.AnalyticFragment;
import com.foodteam.myfood.database.DatabaseHelper;
import com.foodteam.myfood.model.Fridge;
import com.foodteam.myfood.utils.FragmentInteractionListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class AcceptFragment extends AnalyticFragment implements View.OnKeyListener {

    List<Fridge> itemsToAccept;
    EmptyRecyclerView listView;
    RecyclerView.LayoutManager mLayoutManager;
    String type;
    Toolbar toolbar;
    Analytic analytic;
    FriendsToAcceptAdapter adapter;
    ProgressBar loadingBar;
    private FragmentInteractionListener mListener;

    public AcceptFragment() {
    }

    public static AcceptFragment newInstance(String pushType) {
        AcceptFragment fragment = new AcceptFragment();
        Bundle args = new Bundle();

        args.putString(Preferences.PUSH_TYPE, pushType);
        fragment.setArguments(args);
        return fragment;
    }

//    public static AcceptFragment newInstance(String pushType) {
//        AcceptFragment fragment = new AcceptFragment();
//        Bundle args = new Bundle();
//
//        args.putString(ParseFeatures.PUSH_TYPE, pushType);
//        fragment.setArguments(args);
//        return fragment;
//    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getString(Preferences.PUSH_TYPE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_accept_tabs, container, false);
        listView = (EmptyRecyclerView) view.findViewById(R.id.list_fridge_to_accept);
        analytic = getAnalytic();
        mLayoutManager = new LinearLayoutManager(getContext());
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        loadingBar = (ProgressBar) view.findViewById(R.id.loadingBar);
        itemsToAccept = new ArrayList<>();
        listView.setLayoutManager(mLayoutManager);

        View v = view.findViewById(R.id.empty_text);
        listView.setEmptyView(v);

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(this);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        new SetAdapter().execute(type);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (FragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement " + FragmentInteractionListener.class.getSimpleName());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void setInviteAdapter() {
        DatabaseHelper.getInvitesToAccept(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()
                        ) {
                    Fridge fridge = data.getValue(Fridge.class);
                    if (!fridge.isAccepted()) {
                        fridge.setId(data.getKey());
                        itemsToAccept.add(fridge);
                        adapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void setRequestAdapter() {
//        List<ParseObject> usersFridges = new ArrayList<>();
//        ParseQuery<ParseObject> userFridgesQuery = ParseQuery.getQuery(ParseFeatures.FRIDGE);
//        userFridgesQuery.whereEqualTo(ParseFeatures.FRIDGE_AUTHOR, ParseUser.getCurrentUser());
//        try {
//            usersFridges = userFridgesQuery.find();
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        ParseQuery<ParseObject> friendsFridgesQuery = ParseQuery.getQuery(ParseFeatures.USER_FRIDGE);
//        friendsFridgesQuery.whereContainedIn(ParseFeatures.FRIDGE_ID, usersFridges);
//        friendsFridgesQuery.whereEqualTo(ParseFeatures.PERMISSION, false);
//        friendsFridgesQuery.whereNotEqualTo(ParseFeatures.SEND_USER, ParseUser.getCurrentUser());
//        try {
//            itemsToAccept = friendsFridgesQuery.find();
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
        if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
            backToMyFood();
            return true;
        }
        return false;
    }

    private void backToMyFood() {
        if (mListener != null)
            mListener.onBackToMyFood();
    }

    private class SetAdapter extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            loadingBar.setVisibility(View.GONE);
            switch (type) {
                case Preferences.TYPE_ACCEPT_INVITE:
                    toolbar.setTitle(getString(R.string.toolbar_title_invite));
                    break;
                case Preferences.TYPE_ACCEPT_REQUEST:
                    toolbar.setTitle(getString(R.string.toolbar_title_accept));
                    break;
                default:
            }
            listView.setAdapter(adapter);
        }

        @Override
        protected Void doInBackground(String... params) {
            switch (type) {
                case Preferences.TYPE_ACCEPT_INVITE:
                    setInviteAdapter();
                    break;
                case Preferences.TYPE_ACCEPT_REQUEST:
                    setRequestAdapter();
                    break;
                default:
            }
            adapter = new FriendsToAcceptAdapter(itemsToAccept, analytic);
            return null;
        }
    }
}
