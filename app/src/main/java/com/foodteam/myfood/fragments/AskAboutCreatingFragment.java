package com.foodteam.myfood.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.foodteam.myfood.R;
import com.foodteam.myfood.utils.Navigator;

/**
 * Created by Daryna on 20.10.2015.
 */
public class AskAboutCreatingFragment extends Fragment {
    Button buttonCreate, buttonConnect, buttonSkip;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ask_about_creating,
                container, false);
        buttonCreate = (Button) view.findViewById(R.id.button_create);
        buttonConnect = (Button) view.findViewById(R.id.button_connect);
        buttonSkip = (Button) view.findViewById(R.id.skip);
        buttonCreate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Navigator.getInstance().showCreateFridgeFragmentAddBackStack((AppCompatActivity) getActivity());
            }
        });

        buttonConnect.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Navigator.getInstance().showConnectFridgeFragmentAndAddBackStack((AppCompatActivity) getActivity());
            }
        });

        buttonSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().setResult(1);
                getActivity().finish();
            }
        });

        return view;
    }
}
