package com.foodteam.myfood.fragments;


import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.foodteam.myfood.R;
import com.foodteam.myfood.activities.LoginActivity;
import com.foodteam.myfood.adapters.FridgeToSubscribeAdapter;
import com.foodteam.myfood.classes.FacebookFriendsCallback;
import com.foodteam.myfood.customViews.AnalyticFragment;
import com.foodteam.myfood.model.Fridge;
import com.foodteam.myfood.model.User;
import com.foodteam.myfood.singleton.FacebookFriendsList;
import com.foodteam.myfood.utils.FragmentInteractionListener;

import java.util.List;

/**
 * Created by Daryna on 20.10.2015.
 */
public class ConnectFridgeFragment extends AnalyticFragment implements View.OnKeyListener {

    List<User> facebookFriends;
    FacebookFriendsCallback callback;
    ListView listView;
    ProgressBar loadingBar;
    FridgeToSubscribeAdapter adapter;
    private TextView tvTips;
    private List<Fridge> friendFridges;
    private FragmentInteractionListener mListener;

    public ConnectFridgeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_connect_fridge, container, false);

        final Toolbar mToolbar = (Toolbar) view.findViewById(R.id.toolbar);

        if (getActivity() instanceof LoginActivity) {
            ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        } else {
            mToolbar.setVisibility(View.GONE);
        }

        //mRecyclerView = (ListView) view.findViewById(R.id.fridge_to_connect);
        tvTips = (TextView) view.findViewById(android.R.id.empty);
        callback = new FacebookFriendsCallback();
        loadingBar = (ProgressBar) view.findViewById(R.id.loadingBar);
//        facebookFriends = FacebookFriendsList.getInstance();
//
//        ParseQuery<ParseObject> friendsFridgesQuery = ParseQuery.getQuery("Fridge");
//        friendsFridgesQuery.whereContainedIn("author", facebookFriends);
//        friendFridges = new ArrayList<>();
//        try {
//            friendFridges = friendsFridgesQuery.find();
//            ParseFeatures.getFridgeListFromLocal(new FindCallback<ParseObject>() {
//                @Override
//                public void done(List<ParseObject> mRecyclerView, ParseException e) {
//                    friendFridges.removeAll(mRecyclerView);
//                    mAdapter = new FridgeToSubscribeAdapter(getContext(), R.layout.item_fridge_to_subscribe, friendFridges, getAnalytic());
//                    setListAdapter(mAdapter);
//                }
//            });
//
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }


        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(this);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new SetAdapter().execute();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.connect_fridge_fragment, menu);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (FragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement " + FragmentInteractionListener.class.getSimpleName());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle item selection
        switch (item.getItemId()) {
            case R.id.next_button_to_create:
                if (getActivity() instanceof LoginActivity) {
                    getActivity().setResult(1);
                    getActivity().finish();
                } else {
                    backToMyFood();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
        if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
            backToMyFood();
            return true;
        }
        return false;
    }

    private void backToMyFood() {
        if (mListener != null)
            mListener.onBackToMyFood();
    }

    private class SetAdapter extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingBar.setVisibility(View.VISIBLE);
            tvTips.setVisibility(View.INVISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            facebookFriends = FacebookFriendsList.getInstance();


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }
}
