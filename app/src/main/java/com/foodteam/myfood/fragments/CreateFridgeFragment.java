package com.foodteam.myfood.fragments;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.foodteam.myfood.R;
import com.foodteam.myfood.activities.LoginActivity;
import com.foodteam.myfood.activities.MainActivity;
import com.foodteam.myfood.adapters.recycler.FriendsToInviteAdapter;
import com.foodteam.myfood.classes.Analytic;
import com.foodteam.myfood.classes.EmptyRecyclerView;
import com.foodteam.myfood.classes.PrefUtils;
import com.foodteam.myfood.customViews.AnalyticFragment;
import com.foodteam.myfood.database.DatabaseHelper;
import com.foodteam.myfood.model.User;
import com.foodteam.myfood.singleton.FacebookFriendsList;
import com.foodteam.myfood.utils.FragmentInteractionListener;
import com.gc.materialdesign.views.ButtonRectangle;
import com.google.android.gms.appinvite.AppInviteInvitation;
import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

/**
 * Created by Daryna on 20.10.2015.
 */
public class CreateFridgeFragment extends AnalyticFragment implements View.OnKeyListener {
    private EditText fridgeName;
    private EmptyRecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private FriendsToInviteAdapter mAdapter;
    private Analytic mAnalytic;
    private ProgressBar mLoadingBar;
    private ButtonRectangle mButtonInviteToApp;
    private FragmentInteractionListener mListener;

    public CreateFridgeFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_fridge,
                container, false);

        final Toolbar mToolbar = (Toolbar) view.findViewById(R.id.toolbar);

        mAnalytic = getAnalytic();

        if (getActivity() instanceof LoginActivity) {

            ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        } else {
            mToolbar.setVisibility(View.GONE);
        }

        mRecyclerView = (EmptyRecyclerView) view.findViewById(R.id.friends_list);
        fridgeName = (EditText) view.findViewById(R.id.fridge_name_edittext);
        mLoadingBar = (ProgressBar) view.findViewById(R.id.loadingBar);

        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        View v = view.findViewById(R.id.empty_text);
        mRecyclerView.setEmptyView(v);

        mButtonInviteToApp = (ButtonRectangle) view.findViewById(R.id.btn_invite_to_app);
        mButtonInviteToApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onInviteClicked();
            }
        });

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(this);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new SetAdapter().execute();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (FragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement " + FragmentInteractionListener.class.getSimpleName());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.create_fridge_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle item selection
        switch (item.getItemId()) {
            case R.id.next_button_to_create:
                List<User> usersToInvite = mAdapter.getCheckedList();
                PrefUtils prefUtils = new PrefUtils(getContext());
                if (!TextUtils.isEmpty(fridgeName.getText().toString().trim())) {

                    String id = DatabaseHelper.createFridge(getContext(), fridgeName.getText().toString(), usersToInvite);

                    prefUtils.setCurrentFridge(id);

                    mAnalytic.sendEvent("Fridge", "Create");
                    if (getActivity() instanceof LoginActivity) {
                        getActivity().setResult(1);
                        getActivity().finish();
                    } else {
                        backToMyFood();
                    }

                    return true;
                } else {
                    fridgeName.setError(getString(R.string.err_fridge_name));
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
        if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
            if (getActivity() instanceof MainActivity) {
                backToMyFood();
            } else if (getActivity() instanceof LoginActivity) {
                getActivity().setResult(1);
                getActivity().finish();
            }
            return true;
        }
        return false;
    }

    private void backToMyFood() {
        if (mListener != null)
            mListener.onBackToMyFood();
    }

    public void onInviteClicked() {
        Intent intent = new AppInviteInvitation.IntentBuilder(getString(R.string.title_invite_to_app))
                .setMessage(String.format(getString(R.string.msg_invite_to_app), FirebaseAuth.getInstance().getCurrentUser().getDisplayName()))
                .setDeepLink(Uri.parse("https://play.google.com/store/apps/details?id=com.foodteam.myfood"))
                .build();
        startActivityForResult(intent, 333);
    }

    private class SetAdapter extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mLoadingBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            mAdapter = FacebookFriendsList.getAdapter(getContext());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mLoadingBar.setVisibility(View.GONE);
            mRecyclerView.setAdapter(mAdapter);
        }
    }
}
