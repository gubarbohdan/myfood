package com.foodteam.myfood.fragments;


import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.foodteam.myfood.R;
import com.foodteam.myfood.adapters.recycler.FridgeUsersAdapter;
import com.foodteam.myfood.classes.EmptyRecyclerView;
import com.foodteam.myfood.classes.PrefUtils;
import com.foodteam.myfood.customViews.AnalyticFragment;
import com.foodteam.myfood.database.DatabaseHelper;
import com.foodteam.myfood.model.User;
import com.foodteam.myfood.utils.FragmentInteractionListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FridgeUsersFragment extends AnalyticFragment implements View.OnKeyListener {

    private EmptyRecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private FridgeUsersAdapter mAdapter;
    private PrefUtils mPrefUtils;
    private List<User> mFridgeUsersList;
    private ProgressBar mLoadingBar;

    private FragmentInteractionListener mListener;

    public FridgeUsersFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fridge_users, container, false);
        mRecyclerView = (EmptyRecyclerView) view.findViewById(R.id.fridge_users_list);
        mPrefUtils = new PrefUtils(getContext());
        mLoadingBar = (ProgressBar) view.findViewById(R.id.loadingBar);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        View v = view.findViewById(R.id.empty_text);
        mRecyclerView.setEmptyView(v);

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(this);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new SetAdapter().execute();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (FragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement " + FragmentInteractionListener.class.getSimpleName());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.save, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_save) {
            backToMyFood();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
        if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
            backToMyFood();
            return true;
        }
        return false;
    }

    private void backToMyFood() {
        if (mListener != null)
            mListener.onBackToMyFood();
    }

    private class SetAdapter extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mLoadingBar.setVisibility(View.VISIBLE);
            mFridgeUsersList = new ArrayList<>();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mAdapter = new FridgeUsersAdapter(getContext(), mFridgeUsersList, getAnalytic());
            mLoadingBar.setVisibility(View.GONE);
            mRecyclerView.setAdapter(mAdapter);
        }

        @Override
        protected Void doInBackground(Void... params) {
            DatabaseHelper.getUserList(mPrefUtils.getCurrentFridgeId(), new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot data :
                            dataSnapshot.getChildren()) {
                        DatabaseHelper.getUser(data.getKey(), new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                User user = dataSnapshot.getValue(User.class);
                                user.setId(dataSnapshot.getKey());
                                if (!user.getId().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                                    mFridgeUsersList.add(user);
                                    mAdapter.notifyDataSetChanged();
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            /// /ParseFeatures.getFridgeUserList(ParseFeatures.getCurrentFridge(mPrefUtils));
            //mFridgeUsersList.remove(ParseUser.getCurrentUser());


            return null;
        }
    }
}
