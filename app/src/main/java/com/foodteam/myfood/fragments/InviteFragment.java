package com.foodteam.myfood.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.foodteam.myfood.R;
import com.foodteam.myfood.adapters.recycler.FriendsToInviteAdapter;
import com.foodteam.myfood.classes.Analytic;
import com.foodteam.myfood.classes.EmptyRecyclerView;
import com.foodteam.myfood.classes.PrefUtils;
import com.foodteam.myfood.constants.Node;
import com.foodteam.myfood.customViews.AnalyticFragment;
import com.foodteam.myfood.database.DatabaseHelper;
import com.foodteam.myfood.model.Fridge;
import com.foodteam.myfood.model.User;
import com.foodteam.myfood.singleton.FacebookFriendsList;
import com.foodteam.myfood.utils.FragmentInteractionListener;
import com.gc.materialdesign.views.ButtonRectangle;
import com.google.android.gms.appinvite.AppInviteInvitation;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class InviteFragment extends AnalyticFragment implements View.OnKeyListener {

    private Context mContext;
    private EmptyRecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<User> mFriendToInvite;
    private FriendsToInviteAdapter mAdapter;
    private ConnectivityManager cm;
    private PrefUtils mPrefUtils;
    private Analytic mAnalytic;
    private ProgressBar mLoadingBar;
    private ButtonRectangle mButtonInviteToApp;

    private FragmentInteractionListener mListener;

    public InviteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_invite, container, false);
        setHasOptionsMenu(true);

        mContext = getContext();
        mAnalytic = getAnalytic();

        mPrefUtils = new PrefUtils(getContext());

        mFriendToInvite = new ArrayList<>();
        cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        final Toolbar mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        mLoadingBar = (ProgressBar) view.findViewById(R.id.loadingBar);

        mRecyclerView = (EmptyRecyclerView) view.findViewById(R.id.friend_to_invite_list);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        View v = view.findViewById(R.id.empty_text);
        mRecyclerView.setEmptyView(v);

        mButtonInviteToApp = (ButtonRectangle) view.findViewById(R.id.btn_invite_to_app);
        mButtonInviteToApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onInviteClicked();
            }
        });

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(this);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new SetAdapter().execute();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (FragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement " + FragmentInteractionListener.class.getSimpleName());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.create_fridge_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.next_button_to_create:
                if (cm.getActiveNetworkInfo() != null) {
                    final List<User> list = mAdapter.getCheckedList();

                    DatabaseHelper.getFridge(mPrefUtils.getCurrentFridgeId(), new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            Fridge fridge = dataSnapshot.getValue(Fridge.class);
                            fridge.setId(dataSnapshot.getKey());
                            fridge.setUsers(null);
                            fridge.setProducts(null);
                            DatabaseHelper.sendFcmMessage(mContext, list, fridge);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                    mAnalytic.sendEvent("Users", "Invite");
                    getFragmentManager().popBackStack();
                    backToMyFood();
                } else {
                    Toast.makeText(getContext(), "No network", Toast.LENGTH_SHORT).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
        if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
            backToMyFood();
            return true;
        }
        return false;
    }

    private void backToMyFood() {
        if (mListener != null)
            mListener.onBackToMyFood();
    }

    public void onInviteClicked() {
        Intent intent = new AppInviteInvitation.IntentBuilder(getString(R.string.title_invite_to_app))
                .setMessage(String.format(getString(R.string.msg_invite_to_app), FirebaseAuth.getInstance().getCurrentUser().getDisplayName()))
                .setDeepLink(Uri.parse("https://play.google.com/store/apps/details?id=com.foodteam.myfood"))
                .build();
        startActivityForResult(intent, 333);
    }

    private class SetAdapter extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mLoadingBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            //friendsList = FacebookFriendsList.getInstance();
            mAdapter = FacebookFriendsList.getAdapter(getContext());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            deleteCurentFridgeUsers();
            mLoadingBar.setVisibility(View.GONE);
            mRecyclerView.setAdapter(mAdapter);
        }

        private void deleteCurentFridgeUsers() {
            final List<User> users = mAdapter.getFriendsList();
            DatabaseReference dbref = FirebaseDatabase.getInstance().getReference().child(Node.FRIDGES).child(mPrefUtils.getCurrentFridgeId()).child(Node.USERS);
            dbref.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()
                            ) {
                        User user = snapshot.getValue(User.class);
                        user.setId(snapshot.getKey());

                        for (Iterator<User> it = users.iterator(); it.hasNext(); ) {
                            User u = it.next();
                            if (u.getId().equals(user.getId())) {
                                it.remove();
                                mAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }
}
