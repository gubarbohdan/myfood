package com.foodteam.myfood.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.foodteam.myfood.R;
import com.foodteam.myfood.activities.MainActivity;
import com.foodteam.myfood.adapters.ProductsAutocompleteAdapter;
import com.foodteam.myfood.adapters.recycler.MyFoodAdapter;
import com.foodteam.myfood.classes.Analytic;
import com.foodteam.myfood.classes.EmptyRecyclerView;
import com.foodteam.myfood.classes.PrefUtils;
import com.foodteam.myfood.constants.Node;
import com.foodteam.myfood.constants.Preferences;
import com.foodteam.myfood.customViews.AnalyticFragment;
import com.foodteam.myfood.customViews.DelayAutocomplete;
import com.foodteam.myfood.database.DatabaseHelper;
import com.foodteam.myfood.listeners.SaveItemsEventListener;
import com.foodteam.myfood.model.Fridge;
import com.foodteam.myfood.model.Product;
import com.foodteam.myfood.model.ProductItem;
import com.foodteam.myfood.utils.Navigator;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.melnykov.fab.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daryna on 12.10.2015.
 */
public class MyFoodFragment extends AnalyticFragment implements View.OnClickListener {
    private EmptyRecyclerView mMyFoodRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<ProductItem> mFridgeItem;
    private List<ProductItem> mProductsToAdd;
    private PrefUtils mPrefUtils;
    private ChildEventListener mProductListener = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            final ProductItem pItem = dataSnapshot.getValue(ProductItem.class);
            pItem.setId(dataSnapshot.getKey());
            DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference().child(Node.PRODUCTS).child(mPrefUtils.getLocale()).child(dataSnapshot.getKey());
            dbRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Product item = dataSnapshot.getValue(ProductItem.class);
                    item.setId(dataSnapshot.getKey());
                    pItem.setValues(item);
                    mFridgeItem.add(pItem);
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });


        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            for (ProductItem p : mFridgeItem) {
                if (p.getId().equals(dataSnapshot.getKey())) {
                    p.setQuantity(dataSnapshot.child(Node.QUANTITY).getValue(Double.class));
                }
            }
            mAdapter.notifyDataSetChanged();
        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {
            ProductItem removeObject = null;
            for (ProductItem p : mFridgeItem) {
                if (p.getId().equals(dataSnapshot.getKey())) {
                    removeObject = p;
                }
            }
            mFridgeItem.remove(removeObject);
            mAdapter.notifyDataSetChanged();
        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };
    private Fridge mCurrentFridge;
    private Product product;
    private DelayAutocomplete bookTitle;
    private MaterialDialog dialog;
    private AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            product = (Product) parent.getItemAtPosition(position);
            if (mPrefUtils.getSupportedLocales().contains(getResources().getConfiguration().locale.getLanguage())) {
                bookTitle.setText(product.getName());
                ((TextView) dialog.getView().findViewById(R.id.product_dimension)).setText(product.getMeasure());
            } else {
                bookTitle.setText(product.getName());
                ((TextView) dialog.getView().findViewById(R.id.product_dimension)).setText(product.getMeasure());
            }
        }
    };
    private ProgressBar loadingBar;
    private Analytic analytic;
    private View.OnClickListener addListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (bookTitle.getText().toString().trim().equals("")) {
                bookTitle.setError(getString(R.string.err_product));
            } else if (((EditText) dialog.getView().findViewById(R.id.product_count)).getText().toString().trim().equals("")) {
                ((EditText) dialog.getView().findViewById(R.id.product_count)).setError(getString(R.string.err_count));
            } else {
                if (product != null) {

                    ProductItem newProductItem = new ProductItem();
                    newProductItem.setQuantity(Double.parseDouble(((EditText) dialog.getView().findViewById(R.id.product_count)).getText().toString()));
                    newProductItem.setId(product.getId());
                    addToProductList(newProductItem);

                    refreshDialog();
                    analytic.sendEvent("Add product", "Add");
                } else {
                    bookTitle.setError(getString(R.string.err_select));
                }
                product = null;
            }
        }
    };
    private Menu menu;
    private MenuInflater menuInflater;
    private ConnectivityManager cm;

    private FloatingActionButton fabMenu;
    private Toolbar toolbar;
    private MaterialDialog nullDialog;
    private ValueEventListener fridgeListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            mCurrentFridge = dataSnapshot.getValue(Fridge.class);
            if (mCurrentFridge != null) {
                mCurrentFridge.setId(dataSnapshot.getKey());
                mMyFoodRecyclerView.setAdapter(mAdapter);
                toolbar.setTitle(mCurrentFridge.getName());
                if (menu != null) {
                    menu.clear();
                    onCreateOptionsMenu(menu, menuInflater);
                }
                setMenuVisibility(true);
                fabMenu.setVisibility(View.VISIBLE);
                setAdapterFromLocal();
                try {
                    nullDialog.hide();

                } catch (NullPointerException e) {
                    Log.d("null", "Null");
                }
            } else {
                showNullDialog();
            }

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            showNullDialog();
        }
    };

    public MyFoodFragment() {

    }

    private void addToProductList(ProductItem objectToAdd) {
        mProductsToAdd.add(objectToAdd);
    }

    private void refreshDialog() {
        bookTitle.setText("");
        //bookTitle.setError("");
        bookTitle.setError(null);
        ((EditText) dialog.getView().findViewById(R.id.product_count)).setText("");
        ((EditText) dialog.getView().findViewById(R.id.product_count)).setError(null);
        ((TextView) dialog.getView().findViewById(R.id.product_dimension)).setText("");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_food, container, false);

        init(view);

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            nullDialog.hide();
            setCurrentFridge();
        }
    }

    private void init(View view) {
        mMyFoodRecyclerView = (EmptyRecyclerView) view.findViewById(R.id.list_my_food);
        mLayoutManager = new LinearLayoutManager(getContext());
        mMyFoodRecyclerView.setLayoutManager(mLayoutManager);

        View v = view.findViewById(R.id.empty_text);
        mMyFoodRecyclerView.setEmptyView(v);

        //Floating action button

        loadingBar = (ProgressBar) view.findViewById(R.id.loadingBar);

        analytic = getAnalytic();

        //ToDo uncomment when done adding by photo

        fabMenu = (FloatingActionButton) view.findViewById(R.id.multiple_actions);
        fabMenu.setOnClickListener(this);
        mFridgeItem = new ArrayList<>();
        mProductsToAdd = new ArrayList<>();
        //mMyFoodRecyclerView = (ListView) view.findViewById(R.id.my_food_list_view);
        mAdapter = new MyFoodAdapter(getContext(), mFridgeItem, analytic);
        mPrefUtils = new PrefUtils(getContext());
        cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        setHasOptionsMenu(true);
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        fabMenu.attachToRecyclerView(mMyFoodRecyclerView);

        setCurrentFridge();


    }

    private void setCurrentFridge() {
        if (mPrefUtils.getCurrentFridgeId() != null) {
            DatabaseReference fridgeRef = FirebaseDatabase.getInstance().getReference();
            fridgeRef.child(Node.FRIDGES).child(mPrefUtils.getCurrentFridgeId()).addListenerForSingleValueEvent(fridgeListener);
        } else {
            showNullDialog();
        }
    }

    private void showNullDialog() {
        nullDialog = new MaterialDialog.Builder(getContext())
                .title(R.string.dialog_null_fridge_title)
                .content(R.string.dialog_null_fridge_message)
                .positiveText(R.string.btn_ok)
                .show();

        fabMenu.setVisibility(View.GONE);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        this.menu = menu;
        menuInflater = inflater;
        menu.clear();
        inflater.inflate(R.menu.create_fridge, menu);
        if (mCurrentFridge != null) {
            if (FirebaseAuth.getInstance().getCurrentUser() != null && mCurrentFridge.getAuthor().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                inflater.inflate(R.menu.author_of_fridge, menu);
            } else {
                inflater.inflate(R.menu.my_fridge, menu);
            }
        }

    }

    private void setAdapterFromLocal() {
        mFridgeItem.clear();
        //mMyFoodRecyclerView.setAdapter(mAdapter);

        loadingBar.setVisibility(View.GONE);
        DatabaseReference productsRef = FirebaseDatabase.getInstance().getReference().child(Node.FRIDGES).child(mCurrentFridge.getId()).child(Node.PRODUCTS);
        productsRef.addChildEventListener(mProductListener);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.multiple_actions: {
                startAddDialog();
                break;
            }
        }
    }

    private void startAddDialog() {
        dialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.dialog_product_title)
                .customView(R.layout.material_dialogs_custom_view, true)
                .positiveText(R.string.btn_save)
                .negativeText(R.string.btn_cancel)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                        mProductsToAdd.clear();
                    }
                })
                .neutralText(R.string.btn_add)
                .show();
        dialog.getActionButton(DialogAction.POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSaveClick();
            }
        });
        dialog.getActionButton(DialogAction.NEUTRAL).setOnClickListener(addListener);
        setUpAutocompleteField(2, onItemClickListener);
    }

    private void setUpAutocompleteField(int threshold, AdapterView.OnItemClickListener listener) {
        bookTitle = (DelayAutocomplete) dialog.findViewById(R.id.autocomplete_product_name);
        bookTitle.setThreshold(threshold);
        bookTitle.setAdapter(new ProductsAutocompleteAdapter(getContext()));
        bookTitle.setLoadingIndicator((ProgressBar) dialog.findViewById(R.id.progress_bar));
        bookTitle.setOnItemClickListener(listener);
    }

    private void onSaveClick() {
        if (!bookTitle.getText().toString().trim().equals("") && !((EditText) dialog.getView().findViewById(R.id.product_count)).getText().toString().trim().equals("")) {
            if (product != null) {
                ProductItem newProductItem = new ProductItem();
                newProductItem.setQuantity(Double.parseDouble(((EditText) dialog.getView().findViewById(R.id.product_count)).getText().toString()));
                newProductItem.setId(product.getId());
                addToProductList(newProductItem);

                for (ProductItem p : mProductsToAdd
                        ) {
                    final DatabaseReference productRef = FirebaseDatabase.getInstance().getReference().child(Node.FRIDGES).child(mCurrentFridge.getId()).child(Node.PRODUCTS).child(p.getId());
                    productRef.addListenerForSingleValueEvent(new SaveItemsEventListener(productRef, p.getQuantity()));
                }
                dialog.dismiss();
                analytic.sendEvent("Add product", "Save");
                mProductsToAdd.clear();

                //ParseObject productItem = savingParse.getProductItem(product, mCurrentFridge, Double.parseDouble(((EditText) dialog.getView().findViewById(R.id.product_count)).getText().toString()));
                //addToProductList(productItem);
            } else {
                bookTitle.setError(getString(R.string.err_select));
            }
        } else {
            bookTitle.setError(getString(R.string.err_select));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.invite_to_fridge:
                if (cm.getActiveNetworkInfo() != null) {
                    Navigator.getInstance().showInviteFragment((AppCompatActivity) getActivity());
                    return true;
                } else {
                    Toast.makeText(getContext(), R.string.msg_no_network, Toast.LENGTH_SHORT).show();
                    return false;
                }
            case R.id.create_new_fridge:
                if (cm.getActiveNetworkInfo() != null) {
                    Navigator.getInstance().showCreateFridgeFragment((AppCompatActivity) getActivity());
                    return true;
                } else {
                    Toast.makeText(getContext(), R.string.msg_no_network, Toast.LENGTH_SHORT).show();
                    return false;
                }
/*            case R.id.subscribe_to_fridge:
                onInviteClicked();
                return false;*/
            case R.id.unsubscribe_from_fridge:
                if (cm.getActiveNetworkInfo() != null) {
                    MaterialDialog conformDialog = new MaterialDialog.Builder(getContext())
                            .title(R.string.dialog_unsubscribe_title)
                            .positiveText(R.string.btn_ok)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                                    DatabaseHelper.unsubscribeFromFridge(mPrefUtils.getCurrentFridgeId());
                                    mPrefUtils.setCurrentFridge(null);
                                    mMyFoodRecyclerView.setAdapter(null);
                                    //setListAdapter(null);
                                    toolbar.setTitle(R.string.toolbar_title_myfood);
                                    menu.clear();
                                    menuInflater.inflate(R.menu.create_fridge, menu);
                                    onResume();
                                    ((MainActivity) getActivity()).closeFridgeList();
                                }
                            })
                            .negativeText(R.string.btn_cancel)
                            .content(R.string.dialog_unsubscribe_message)
                            .show();


                    return true;
                } else {
                    Toast.makeText(getContext(), R.string.msg_no_network, Toast.LENGTH_SHORT).show();
                    return false;
                }
            case R.id.edit_fridge_users:
                if (cm.getActiveNetworkInfo() != null) {
                    Navigator.getInstance().showFridgeUsersFragment((AppCompatActivity) getActivity());
                    return true;
                } else {
                    Toast.makeText(getContext(), R.string.msg_no_network, Toast.LENGTH_SHORT).show();
                    return false;
                }
            case R.id.requests:
                if (cm.getActiveNetworkInfo() != null) {
                    Navigator.getInstance().showAcceptFragment((AppCompatActivity) getActivity(), Preferences.TYPE_ACCEPT_INVITE);
                    //Navigator.getInstance().showRequestFragment((AppCompatActivity) getActivity());
                    return true;
                } else {
                    Toast.makeText(getContext(), R.string.msg_no_network, Toast.LENGTH_SHORT).show();
                    return false;
                }
            case R.id.delete_fridge:
                MaterialDialog conformDialog = new MaterialDialog.Builder(getContext())
                        .title(R.string.dialog_deletefridge_title)
                        .positiveText(R.string.btn_ok)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                                DatabaseHelper.deleteFridge(mPrefUtils.getCurrentFridgeId());
                                mPrefUtils.setCurrentFridge(null);
                                mMyFoodRecyclerView.setAdapter(null);
                                //setListAdapter(null);
                                toolbar.setTitle(R.string.toolbar_title_myfood);
                                menu.clear();
                                menuInflater.inflate(R.menu.create_fridge, menu);
                                fabMenu.setVisibility(View.GONE);
                                showNullDialog();
                                onResume();
                                ((MainActivity) getActivity()).closeFridgeList();
                            }
                        })
                        .negativeText(R.string.btn_cancel)
                        .content(R.string.dialog_deletefridge_message)
                        .show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
