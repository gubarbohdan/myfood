package com.foodteam.myfood.fragments;


/**
 * Created by Daryna on 27.10.2015.
 */

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.foodteam.myfood.R;
import com.foodteam.myfood.adapters.recycler.RecipiesAdapter;
import com.foodteam.myfood.classes.PrefUtils;
import com.foodteam.myfood.database.DatabaseHelper;
import com.foodteam.myfood.model.Recipe;
import com.foodteam.myfood.utils.Navigator;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class RecipeTabFragment extends Fragment {

    private static final String ARG_POSITION = "categoryID";

    private String categoryId;
    private RecyclerView listView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecipiesAdapter adapter;
    private List<Recipe> recipies;
    private PrefUtils prefUtils;
    private ConnectivityManager cm;

    private ValueEventListener listener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            for (DataSnapshot data : dataSnapshot.getChildren()
                    ) {
                Recipe recipe = data.getValue(Recipe.class);
                recipe.setId(data.getKey());
                recipies.add(recipe);
                adapter.notifyDataSetChanged();
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    public static RecipeTabFragment newInstance(String categoryId) {
        RecipeTabFragment fragment = new RecipeTabFragment();
        Bundle args = new Bundle();
        args.putString(ARG_POSITION, categoryId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefUtils = new PrefUtils(getContext());
        if (getArguments() != null)
            categoryId = getArguments().getString(ARG_POSITION);
        cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_recipe_tab, container, false);

        listView = (RecyclerView) view.findViewById(R.id.recipiesListView);
        mLayoutManager = new LinearLayoutManager(getContext());
        listView.setLayoutManager(mLayoutManager);
        recipies = new ArrayList<>();
        adapter = new RecipiesAdapter(getContext(), recipies, new RecipiesAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if (cm.getActiveNetworkInfo() != null) {
                    Navigator.getInstance().showRecipesDetailsFragment(
                            (AppCompatActivity) getActivity(),
                            adapter.getItem(position).getId(), categoryId);
                } else {
                    Toast.makeText(getContext(), R.string.msg_no_network, Toast.LENGTH_SHORT).show();
                }
            }
        });
        listView.setAdapter(adapter);

        getRecipiesList();

        return view;
    }

    private void getRecipiesList() {

        DatabaseHelper.getRecipesByCategory(categoryId, prefUtils.getLocale(), listener);

    }
}
