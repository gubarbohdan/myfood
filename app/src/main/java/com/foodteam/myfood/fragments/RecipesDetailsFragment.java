package com.foodteam.myfood.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.foodteam.myfood.R;
import com.foodteam.myfood.activities.MainActivity;
import com.foodteam.myfood.adapters.DishComponentsAdapter;
import com.foodteam.myfood.classes.FirebaseImageLoader;
import com.foodteam.myfood.classes.PrefUtils;
import com.foodteam.myfood.constants.Node;
import com.foodteam.myfood.customViews.AnalyticFragment;
import com.foodteam.myfood.database.DatabaseHelper;
import com.foodteam.myfood.model.ProductItem;
import com.foodteam.myfood.model.Recipe;
import com.foodteam.myfood.utils.FragmentInteractionListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daryna on 26.11.2015.
 */
public class RecipesDetailsFragment extends AnalyticFragment implements View.OnKeyListener {
    ListView listViewComponent;
    List<ProductItem> dishcomponent;
    TextView recipesDescription;
    //TextView recipeName;
    ImageView recipeImage;
    DishComponentsAdapter dishAdapter;
    String recipesId;
    String categoryId;
    Recipe currentRecipe;
    PrefUtils prefUtils;
    ValueEventListener recipeListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            currentRecipe = dataSnapshot.getValue(Recipe.class);
            ((MainActivity) getActivity()).getSupportActionBar().setTitle(currentRecipe.getName());
            recipesDescription.setText(currentRecipe.getDescription());

            StorageReference ref = DatabaseHelper.getRecipeImageRef(currentRecipe.getPhoto());
            Glide.with(getContext()).using(new FirebaseImageLoader()).load(ref).centerCrop().placeholder(R.drawable.test_icon).into(recipeImage);
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };
    private ProgressBar loadingBar;
    ValueEventListener productListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            for (DataSnapshot data :
                    dataSnapshot.getChildren()) {
                ProductItem item = dataSnapshot.getValue(ProductItem.class);
                item.setId(data.getKey());
                item.setQuantity(data.child(Node.QUANTITY).getValue(Double.class));
                dishcomponent.add(item);
            }
            dishAdapter = new DishComponentsAdapter(getContext(), R.layout.item_dish_components, dishcomponent);
            listViewComponent.setAdapter(dishAdapter);
            loadingBar.setVisibility(View.GONE);
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };
    private FragmentInteractionListener mListener;

    public RecipesDetailsFragment() {
    }

    public static RecipesDetailsFragment newInstance(String recipesId, String categoryId) {
        RecipesDetailsFragment fragment = new RecipesDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("RecipesID", recipesId);
        bundle.putString("CategoryID", categoryId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            recipesId = getArguments().getString("RecipesID");
            categoryId = getArguments().getString("CategoryID");
        }
        prefUtils = new PrefUtils(getContext());
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recipes_details, container, false);
        listViewComponent = (ListView) view.findViewById(R.id.dish_components_listview);
        loadingBar = (ProgressBar) view.findViewById(R.id.loadingBar);
        recipesDescription = (TextView) view.findViewById(R.id.recipes_description);
        //recipeName = (TextView) view.findViewById(R.id.dish_name);
        recipeImage = (ImageView) view.findViewById(R.id.recipes_icon);

        loadingBar.setVisibility(View.VISIBLE);
        dishcomponent = new ArrayList<>();


        setRecipe();


        //listViewComponent.setOnClickListener(null);

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(this);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (FragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement " + FragmentInteractionListener.class.getSimpleName());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void setRecipe() {

        DatabaseHelper.getRecipeById(categoryId, recipesId, prefUtils.getLocale(), recipeListener, productListener);

    }

    @Override
    public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
        if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
            backToRecipes();
            return true;
        }
        return false;
    }


    private void backToRecipes() {
        if (mListener != null)
            mListener.onBackToRecipes();
    }

}
