package com.foodteam.myfood.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.astuetz.PagerSlidingTabStrip;
import com.foodteam.myfood.R;
import com.foodteam.myfood.classes.PrefUtils;
import com.foodteam.myfood.database.DatabaseHelper;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecipesFragment extends Fragment {

    private PagerSlidingTabStrip tabs;
    private ViewPager pager;
    private MyPagerAdapter adapter;
    private PrefUtils prefUtils;
    private List<String> TITLES;
    ValueEventListener listener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            for (DataSnapshot data : dataSnapshot.getChildren()
                    ) {
                String category = data.getKey();
                TITLES.add(category);
            }
            adapter = new MyPagerAdapter(getChildFragmentManager());
            final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                    .getDisplayMetrics());
            pager.setPageMargin(pageMargin);
            pager.setAdapter(adapter);
            tabs.setViewPager(pager);
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    public RecipesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_recipies, container, false);
        prefUtils = new PrefUtils(getContext());
        TITLES = new ArrayList<>();
        tabs = (PagerSlidingTabStrip) view.findViewById(R.id.tabs);

        DatabaseHelper.getCategories(prefUtils.getLocale(), listener);

        pager = (ViewPager) view.findViewById(R.id.pager);


        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.toolbar_title_recipes);


        return view;
    }

//    private void setupToolbar(View view) {
//        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
//        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
//        appCompatActivity.setSupportActionBar(toolbar);
//        if (appCompatActivity.getSupportActionBar() != null) {
//            appCompatActivity.getSupportActionBar().setTitle(R.string.sign_in);
//            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    onBackPressed();
//                }
//            });
//        }
//    }

    public class MyPagerAdapter extends FragmentPagerAdapter {





/*        SaveCallback saveCallback = new SaveCallback() {
            @Override
            public void done(ParseException e) {
                ParseFeatures.getRecipesCategoryFromLocal(findCallback);
            }
        };*/

/*        FindCallback<ParseObject> findCallback = new FindCallback<ParseObject>() {
            @Override
            public void done(List mRecyclerView, ParseException e) {
                mPrefUtils.setCategoriesUpdated();
                TITLES = mRecyclerView;
                pager.setAdapter(mAdapter);

                final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                        .getDisplayMetrics());
                pager.setPageMargin(pageMargin);

                tabs.setViewPager(pager);
            }
        };*/

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);

            //DatabaseHelper.getCategories(mPrefUtils.getLocale(), listener);


        }


        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES.get(position);
        }

        @Override
        public int getCount() {
            return TITLES.size();
        }

        @Override
        public Fragment getItem(int position) {
            return RecipeTabFragment.newInstance(TITLES.get(position));
        }

    }


}
