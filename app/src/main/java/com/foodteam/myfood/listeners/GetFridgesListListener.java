package com.foodteam.myfood.listeners;

import android.widget.ArrayAdapter;

import com.foodteam.myfood.model.Fridge;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

import java.util.List;

/**
 * Created by gubar on 03.02.2017.
 */

public class GetFridgesListListener implements ChildEventListener {
    List<Fridge> mFridgeList;
    ArrayAdapter<Fridge> mAdapter;


    public GetFridgesListListener(List<Fridge> list, ArrayAdapter<Fridge> adapter) {
        mFridgeList = list;
        this.mAdapter = adapter;
    }

    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        Fridge fridge = dataSnapshot.getValue(Fridge.class);
        fridge.setId(dataSnapshot.getKey());
        if (fridge.isAccepted()) {
            mFridgeList.add(fridge);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }
}
