package com.foodteam.myfood.listeners;

import com.foodteam.myfood.model.ProductItem;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by gubar on 02.02.2017.
 */

public class SaveItemsEventListener implements ValueEventListener {
    private DatabaseReference mProductRef;
    private Double mQuantity;

    public SaveItemsEventListener(DatabaseReference reference, Double quantity) {
        mProductRef = reference;
        this.mQuantity = quantity;
    }


    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        ProductItem productItem = dataSnapshot.getValue(ProductItem.class);
        if (productItem == null) {
            productItem = new ProductItem();
            productItem.setQuantity(mQuantity);
        } else {
            productItem.setQuantity(productItem.getQuantity() + mQuantity);
        }
        mProductRef.setValue(productItem);
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }
}
