package com.foodteam.myfood.model;

import java.util.Map;

/**
 * Created by gubar on 09.11.2016.
 */

public class Fridge {

    private String id;
    private Map<String, ProductItem> products;
    private String author;
    private Map<String, User> users;
    private String name;
    private boolean accepted;

    public Fridge() {
    }

    public Fridge(String id, Map<String, ProductItem> products, String author, String name) {
        this.id = id;
        this.products = products;
        this.author = author;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<String, ProductItem> getProducts() {
        return products;
    }

    public void setProducts(Map<String, ProductItem> products) {
        this.products = products;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Map<String, User> getUsers() {
        return users;
    }

    public void setUsers(Map<String, User> users) {
        this.users = users;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }
}
