package com.foodteam.myfood.model;

/**
 * Created by gubar on 09.11.2016.
 */

public class Product {

    private String id;
    private String name;
    private Double kCal;
    private Double protein;
    private Double carbohydrt;
    private Double lipid;
    private String photo;
    private String measure;
    private String description;

    public Product() {
    }

    public Product(String id, String name, double kcal, double protein, double carbonoHydrates, double lipid, String photoUrl, String measure, String description) {
        this.id = id;
        this.name = name;
        kCal = kcal;
        this.protein = protein;
        this.carbohydrt = carbonoHydrates;
        this.lipid = lipid;
        this.photo = photoUrl;
        this.measure = measure;
        this.description = description;
    }

    public Product(Product product) {
        this.id = product.getId();
        this.name = product.getName();
        this.kCal = product.getkCal();
        this.protein = product.getProtein();
        this.carbohydrt = product.getCarbohydrt();
        this.lipid = product.getLipid();
        this.photo = product.getPhoto();
        this.measure = product.getMeasure();
        this.description = product.getDescription();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getkCal() {
        return kCal;
    }

    public void setkCal(Double kCal) {
        this.kCal = kCal;
    }

    public Double getProtein() {
        return protein;
    }

    public void setProtein(Double protein) {
        this.protein = protein;
    }

    public Double getCarbohydrt() {
        return carbohydrt;
    }

    public void setCarbohydrt(Double carbohydrt) {
        this.carbohydrt = carbohydrt;
    }

    public Double getLipid() {
        return lipid;
    }

    public void setLipid(Double lipid) {
        this.lipid = lipid;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setValues(Product product) {
        this.id = product.getId();
        this.name = product.getName();
        this.kCal = product.getkCal();
        this.protein = product.getProtein();
        this.carbohydrt = product.getCarbohydrt();
        this.lipid = product.getLipid();
        this.photo = product.getPhoto();
        this.measure = product.getMeasure();
        this.description = product.getDescription();
    }
}
