package com.foodteam.myfood.model;

/**
 * Created by gubar on 11.11.2016.
 */

public class ProductItem extends Product {

    private Double quantity;

    public ProductItem() {
        super();
    }

    public ProductItem(String id, String name, double kcal, double protein, double carbonoHydrates, double lipid, String photoUrl, String measure, String description, double quantity) {
        super(id, name, kcal, protein, carbonoHydrates, lipid, photoUrl, measure, description);
        this.quantity = quantity;
    }

    public ProductItem(Product product) {
        super(product);
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }
}
