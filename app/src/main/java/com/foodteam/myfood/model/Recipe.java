package com.foodteam.myfood.model;

import java.util.Map;

/**
 * Created by gubar on 09.11.2016.
 */

public class Recipe {

    private String id;
    private String name;
    private String description;
    private String photo;
    private RecipeCategory recipeCategory;
    private Map<String, Product> products;

    public Recipe() {
    }

    public Recipe(String id, String name, String description, String photo, RecipeCategory recipeCategory, Map<String, Product> products) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.photo = photo;
        this.recipeCategory = recipeCategory;
        this.products = products;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public RecipeCategory getRecipeCategory() {
        return recipeCategory;
    }

    public void setRecipeCategory(RecipeCategory recipeCategory) {
        this.recipeCategory = recipeCategory;
    }

    public Map<String, Product> getProducts() {
        return products;
    }

    public void setProducts(Map<String, Product> products) {
        this.products = products;
    }
}
