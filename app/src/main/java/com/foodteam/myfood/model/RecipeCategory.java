package com.foodteam.myfood.model;

/**
 * Created by gubar on 09.11.2016.
 */

public enum RecipeCategory {
    SALAD,
    GRILED,
    BOILED,
    JUICE
}
