package com.foodteam.myfood.model;

import java.util.Map;

/**
 * Created by gubar on 09.11.2016.
 */

public class User {
    private String id;
    private boolean accepted;
    private String name;
    private String facebookId;
    private String photo;
    private String fcmToken;
    private Map<String, Fridge> fridges;

    public User() {
    }

    public User(String id, boolean accepted, String name, String facebookId, String photo, String fcmToken) {
        this.id = id;
        this.accepted = accepted;
        this.name = name;
        this.facebookId = facebookId;
        this.photo = photo;
        this.fcmToken = fcmToken;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public Map<String, Fridge> getFridges() {
        return fridges;
    }

    public void setFridges(Map<String, Fridge> fridges) {
        this.fridges = fridges;
    }
}
