package com.foodteam.myfood.model.messages;

import android.content.Context;

import com.foodteam.myfood.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gubar on 2/2/2017.
 */

public class Message {
    transient Context context;

    private List<String> registration_ids;
    private Data data;

    public Message(Context context, List<User> to, String body) {
        this.context = context;
        registration_ids = new ArrayList<>();
        for (User u : to
                ) {
            registration_ids.add(u.getFcmToken());
        }
        data = new Data(body);
    }

    public class Data {
        private boolean openRequests;
        private String body;

        public Data(String body) {
            this.openRequests = true;
            this.body = body;
        }
    }
}
