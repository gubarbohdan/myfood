package com.foodteam.myfood.services;

import com.foodteam.myfood.database.DatabaseHelper;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by gubar on 2/2/2017.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        DatabaseHelper.setFcmToken();
    }
}
