package com.foodteam.myfood.singleton;

import android.content.Context;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.foodteam.myfood.adapters.recycler.FriendsToInviteAdapter;
import com.foodteam.myfood.constants.FacebookConstants;
import com.foodteam.myfood.constants.Node;
import com.foodteam.myfood.model.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gubar on 13.10.2015.
 */
public class FacebookFriendsList extends ArrayList<User> implements GraphRequest.Callback {
    private static volatile List<User> instance;
    private static GraphRequest.Callback callback;
    private static FriendsToInviteAdapter adapter;

    private FacebookFriendsList() {
        super();

    }

    public static List<User> getInstance() {
        List<User> localInstance = instance;
        Thread t = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            makeFriendsRequest();
                        }
                    });
                    t.start();
                    try {
                        t.join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    //instance = localInstance = callback.getFriendUsers();
        return localInstance;
    }

    private static void makeFriendsRequest() {
    /* make the API call */
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                FacebookConstants.FACEBOOK_REQUEST_FRIENDS,
                null,
                HttpMethod.GET,
                new FacebookFriendsList()
        ).executeAndWait();
    }

    public static FriendsToInviteAdapter getAdapter(Context context) {
        getInstance();
        adapter = new FriendsToInviteAdapter(context, instance);
        return adapter;
    }

    @Override
    public void onCompleted(GraphResponse response) {
        //ToDo make users request
        JSONObject friendsRequest = response.getJSONObject();
        instance = new ArrayList<>();
        try {
            JSONArray friendsJSONArray = friendsRequest.getJSONArray(FacebookConstants.FACEBOOK_RESPONSE_DATA);
            final List<String> friendsList = new ArrayList<String>();
            for (int i = 0; i < friendsJSONArray.length(); i++) {
                friendsList.add(friendsJSONArray.getJSONObject(i).getString(FacebookConstants.FACEBOOK_RESPONSE_ID));
            }

            final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(Node.USERS);
            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        User user = data.getValue(User.class);
                        user.setId(data.getKey());
                        if (friendsList.contains(user.getFacebookId())) {
                            instance.add(user);
                            adapter.notifyDataSetChanged();
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (JSONException ex) {

        }
    }
}
