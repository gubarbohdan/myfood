package com.foodteam.myfood.utils;

/**
 * Created by Daryna on 25.12.2015.
 */
public interface FragmentInteractionListener {
    void onBackToMyFood();

    void onBackToRecipes();
}
