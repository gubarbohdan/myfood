package com.foodteam.myfood.utils;

import android.support.v7.app.AppCompatActivity;

import com.foodteam.myfood.R;
import com.foodteam.myfood.fragments.AcceptFragment;
import com.foodteam.myfood.fragments.AskAboutCreatingFragment;
import com.foodteam.myfood.fragments.ConnectFridgeFragment;
import com.foodteam.myfood.fragments.CreateFridgeFragment;
import com.foodteam.myfood.fragments.FridgeUsersFragment;
import com.foodteam.myfood.fragments.InviteFragment;
import com.foodteam.myfood.fragments.LoginFragment;
import com.foodteam.myfood.fragments.MyFoodFragment;
import com.foodteam.myfood.fragments.RecipesDetailsFragment;
import com.foodteam.myfood.fragments.RecipesFragment;
import com.foodteam.myfood.fragments.RequestFragment;

/**
 * Created by Daryna on 25.12.2015.
 */
public class Navigator {

    private static Navigator mInstance = null;

    private Navigator() {

    }

    public static Navigator getInstance() {
        if (mInstance == null) {
            mInstance = new Navigator();
        }
        return mInstance;
    }

    public void showLoginFragment(AppCompatActivity activity) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.login_fragments_container, new LoginFragment())
                .commit();
    }

    public void showAskAboutCreatingFragment(AppCompatActivity activity) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.login_fragments_container, new AskAboutCreatingFragment())
                .commit();
    }

    public void showCreateFridgeFragment(AppCompatActivity activity) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_activity_content_frame, new CreateFridgeFragment())
                .commit();
    }

    public void showCreateFridgeFragmentAddBackStack(AppCompatActivity activity) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.login_fragments_container, new CreateFridgeFragment())
                .addToBackStack(CreateFridgeFragment.class.getSimpleName())
                .commit();
    }

    public void showConnectFridgeFragment(AppCompatActivity activity) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_activity_content_frame, new ConnectFridgeFragment())
                .commit();
    }

    public void showConnectFridgeFragmentAndAddBackStack(AppCompatActivity activity) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.login_fragments_container, new ConnectFridgeFragment())
                .addToBackStack(ConnectFridgeFragment.class.getSimpleName())
                .commit();
    }

    public void showMyFoodFragment(AppCompatActivity activity) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_activity_content_frame, new MyFoodFragment())
                .commit();
    }

    public void showInviteFragment(AppCompatActivity activity) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_activity_content_frame, new InviteFragment())
                .commit();
    }

    public void showFridgeUsersFragment(AppCompatActivity activity) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_activity_content_frame, new FridgeUsersFragment())
                .commit();
    }

    public void showAcceptFragment(AppCompatActivity activity, String pushType) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_activity_content_frame, AcceptFragment.newInstance(pushType))
                .commit();
    }

    public void showRecipesFragment(AppCompatActivity activity) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_activity_content_frame, new RecipesFragment())
                .commit();
    }

    public void showRecipesDetailsFragment(AppCompatActivity activity, String recipeId, String categoryId) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_activity_content_frame, RecipesDetailsFragment.newInstance(recipeId, categoryId))
                .addToBackStack(RecipesDetailsFragment.class.getSimpleName())
                .commit();
    }

    public void showRequestFragment(AppCompatActivity activity) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_activity_content_frame, new RequestFragment())
                .commit();
    }
}
